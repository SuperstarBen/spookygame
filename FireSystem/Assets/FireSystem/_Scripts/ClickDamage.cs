﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickDamage : MonoBehaviour
{
    private LayerMask mask;

    void Start() {
        mask = LayerMask.GetMask("FireCell");
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit[] hit;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * 50000000, Color.red);
            hit = Physics.RaycastAll(ray, 1000, mask);
            if (hit.Length > 0) {
                FireCell fireCell = hit[hit.Length - 1].collider.gameObject.GetComponent<FireCell>();
                fireCell.TakeDamage(10);
                Debug.Log("Dealt 10 Damage to Fire Cell " + fireCell.name);
            }
        }
    }
}

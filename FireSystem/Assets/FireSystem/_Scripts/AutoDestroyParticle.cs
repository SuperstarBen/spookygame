﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyParticle : MonoBehaviour
{
    void Update()
    {
        if (GetComponent<ParticleSystem>().isEmitting)
        {

        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}

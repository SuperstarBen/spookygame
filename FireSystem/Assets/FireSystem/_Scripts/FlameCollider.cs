﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameCollider : MonoBehaviour {

    private MeshCollider meshCollider;

    public HashSet<GameObject> objectHashSet = new HashSet<GameObject>();
    public List<FireCell> collidersToDamage = new List<FireCell>();

    public void SetupCollider(MeshCollider col) {
        this.meshCollider = col;
    }

    public void CheckColliders(Collider col) {
        Physics.IgnoreCollision(meshCollider, col, false);
    }

    public void IgnoreColliders(Collider col) {
        Physics.IgnoreCollision(meshCollider, col);
    }

    public void IgnoreColliders(List<Collider> colList) {
        for (int i = 0; i<colList.Count; i++) {
            Physics.IgnoreCollision(meshCollider, colList[i]);
        }
    }

    public void IgnoreColliders(List<FireCell> fireCells) {
        for (int i = 0; i < fireCells.Count; i++) {
            Physics.IgnoreCollision(meshCollider, fireCells[i].boxCollider);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (objectHashSet.Contains(other.gameObject)) return;

        if (other.gameObject.layer == LayerMask.NameToLayer("FireCell")) {
            FireCell cell = other.gameObject.GetComponent<FireCell>();

            if (cell.isBurning || cell.currentFuel <= 0) {
                IgnoreColliders(cell.boxCollider);
            } else {
                objectHashSet.Add(other.gameObject);
                collidersToDamage.Add(other.gameObject.GetComponent<FireCell>());
            }
        } else {
            IgnoreColliders(other);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("FireCell")) {
            FireCell cell = other.gameObject.GetComponent<FireCell>();

            objectHashSet.Remove(other.gameObject);
            collidersToDamage.Remove(cell);
        }
    }

    private void OnCollisionEnter(Collision collision) {
        IgnoreColliders(collision.collider);
    }
}

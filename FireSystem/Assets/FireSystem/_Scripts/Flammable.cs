﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Flammable : MonoBehaviour {

    [SerializeField] private float CELL_SIZE = 0.2f;

    private const float DAMAGE_INTERVAL = 0.5f;
    private const float BURN_INTERVAL = 0.1f;

    [SerializeField] private GameObject _fireCell;

    private List<FireCell> _fireGrid = new List<FireCell>();

    private Rigidbody _rb;
    private bool _isBurning;

    private MeshRenderer _meshRenderer;
    private Coroutine _adjustBurnAmountCoroutine;

    [SerializeField] private float _percentageIntensity;
    public float percentageIntensity { get => _percentageIntensity; }

    [SerializeField] private float _percentageBurnt;
    public float percentageBurnt { get => _percentageBurnt; }

    [SerializeField] private float _fireCellHp;
    [SerializeField] private float _fireCellFuel;
    [SerializeField] private AnimationCurve _fireCellIntensityCurve;
    [SerializeField] private float _fireCellIntensityModifier;
    [SerializeField] private float _fireCellDamageToOtherObjects;
    [SerializeField] private float _fireCellDamageDistanceMultiplier;

    private List<Collider> _colliders;
    public List<Collider> colliders { get => _colliders; }

    [System.Serializable]
    public class Flame {
        private List<FireCell> _cells = new List<FireCell>();
        public List<FireCell> cells { get => _cells; }

        private GameObject _firePfx;
        private Transform _firePfxTransform;
        private Flammable _parent;

        private Mesh _mesh;
        public Mesh mesh { get => _mesh; }

        private GameObject _flameColliderObject;
        private MeshCollider _flameMeshCollider;
        private FlameCollider _flameCollider;
        public FlameCollider flameCollider { get => _flameCollider; }

        public Flame(GameObject firePfx, Flammable parent) {
            this._firePfx = firePfx;
            this._firePfxTransform = firePfx.transform;

            this._parent = parent;

            _firePfxTransform.SetParent(parent.transform, false);
            _firePfxTransform.localPosition = Vector3.zero;

            _flameColliderObject = new GameObject();
            _flameColliderObject.name = _parent.name + "_FlameCollider";
            _flameColliderObject.transform.SetParent(_parent.transform, false);
            _flameColliderObject.transform.localPosition = Vector3.zero;

            _flameColliderObject.AddComponent<Rigidbody>().useGravity = false;
            _flameColliderObject.GetComponent<Rigidbody>().isKinematic = true;
            _flameMeshCollider = _flameColliderObject.AddComponent<MeshCollider>();
            //_flameMeshCollider.isTrigger = true;
            _flameCollider = _flameColliderObject.AddComponent<FlameCollider>();
            _flameCollider.SetupCollider(_flameMeshCollider);

            _flameCollider.IgnoreColliders(_parent._fireGrid);
            _flameCollider.IgnoreColliders(_parent._colliders);

            AdjustFire();
        }

        private void AdjustFire() {
            ParticleSystem.EmissionModule emission = _firePfx.GetComponent<ParticleSystem>().emission;
            ParticleSystem.ShapeModule shape = _firePfx.GetComponent<ParticleSystem>().shape;

            _mesh = CombineCells();

            _mesh.name = "FireParticleMesh";
            shape.mesh = _mesh;
            //shape.position = new Vector3(0f, 0.05f, 0f);
            shape.scale = _parent.transform.localScale;

            _flameMeshCollider.sharedMesh = _mesh;
            _flameColliderObject.transform.localScale = new Vector3(1f, 1f, 1f) * _parent._fireCellDamageDistanceMultiplier;

            int emissionRate = 0;
            for (int i = 0; i < cells.Count; i++) {
                emissionRate += (int) (20f * (_parent.CELL_SIZE * 20f) * cells[i].currentIntensity);
            }
            if (emissionRate < 5) emissionRate = 5;
            emission.rateOverTime = emissionRate;
        }

        #region PRIVATE_UTIL_METHODS
        private Mesh CombineCells() {
            CombineInstance[] combine = new CombineInstance[cells.Count];
            for (int i = 0; i < cells.Count; i++) {
                combine[i].subMeshIndex = 0;
                combine[i].mesh = cells[i].GetComponent<MeshFilter>().sharedMesh;
                Matrix4x4 myTransform = _parent.GetComponent<MeshFilter>().transform.worldToLocalMatrix;
                combine[i].transform = myTransform * cells[i].GetComponent<MeshFilter>().transform.localToWorldMatrix;
            }

            Mesh mesh = new Mesh();
            mesh.CombineMeshes(combine);

            return mesh;
        }

        private Vector3 GetFlameAveragePosition() {
            Vector3 pos = Vector3.zero;
            for (int i = 0; i < cells.Count; i++) {
                Transform trans = cells[i].transform;
                pos += trans.position;
            }
            pos /= cells.Count;

            return pos;
        }

        private Vector3 GetFlameAverageSize() {
            Vector3 size = Vector3.zero;

            Vector3 minVal = cells[0].transform.position;
            Vector3 maxVal = cells[0].transform.position;
            for (int i = 0; i < cells.Count; i++) {
                Transform trans = cells[i].transform;

                if (trans.position.x < minVal.x) minVal.x = trans.position.x;
                if (trans.position.y < minVal.y) minVal.y = trans.position.y;
                if (trans.position.z < minVal.z) minVal.z = trans.position.z;

                if (trans.position.x > maxVal.x) maxVal.x = trans.position.x;
                if (trans.position.y > maxVal.y) maxVal.y = trans.position.y;
                if (trans.position.z > maxVal.z) maxVal.z = trans.position.z;
            }

            size = maxVal - minVal;

            return size;
        }
        #endregion

        #region PUBLIC_UTIL_METHODS
        public void AddCell(FireCell cell) {
            cells.Add(cell);
            AdjustFire();
        }

        public void AddCellRange(List<FireCell> cell) {
            for (int i = 0; i < cell.Count; i++) {
                cells.Add(cell[i]);
            }

            AdjustFire();
        }

        public void RemoveCell(FireCell cell) {
            cells.Remove(cell);

            if (cells.Count > 0) AdjustFire();
            else Destroy();
        }

        public void Destroy() {
            PoolManager.instance.DisableParticles(_firePfx);
            _parent.flames.Remove(this);
        }
        #endregion
    }

    [SerializeField] List<Flame> flames = new List<Flame>();

    void Awake() {
        _rb = GetComponent<Rigidbody>();
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    void Start() {
        StartCoroutine(SetupCoroutine());
    }

    private IEnumerator SetupCoroutine() {
        SetupFireCells();

        yield return Yielders.Get(0.5f);

        SetupAdjacentCells();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            SetupAdjacentCells();
        }
    }

    private List<Collider> GetColliders() {
        Transform colliderParent = null;
        for (int i = 0; i < transform.childCount; i++) {
            if (transform.GetChild(i).name == "Colliders") {
                colliderParent = transform.GetChild(i);
            }
        }

        List<Collider> colliders = new List<Collider>();
        for (int i = 0; i < colliderParent.childCount; i++) {
            colliders.Add(colliderParent.GetChild(i).GetComponent<Collider>());
        }

        return colliders;
    }

    private void SetupFireCells() {
        _colliders = GetColliders();

        int count = 0;
        for (int i = 0; i < _colliders.Count; i++) {
            Bounds meshBounds = _colliders[i].bounds;
            Vector3 min = meshBounds.min;
            Vector3 max = meshBounds.max;

            for (float x = min.x; x < max.x; x = x + CELL_SIZE) {
                for (float y = min.y; y < max.y; y = y + CELL_SIZE) {
                    for (float z = min.z; z < max.z; z = z + CELL_SIZE) {
                        Vector3 offset = new Vector3(CELL_SIZE / 2, CELL_SIZE / 2, CELL_SIZE / 2);
                        Vector3 pos = new Vector3(x, y, z) + offset;

                        Collider[] testColliders = Physics.OverlapBox(pos, offset / 2);
                        if (testColliders.Length > 0) {
                            GameObject cell = Instantiate(_fireCell, pos, Quaternion.identity);
                            FireCell script = cell.GetComponent<FireCell>();

                            script.SetupObject(_fireCellHp, _fireCellFuel, _fireCellIntensityModifier, CELL_SIZE, _fireCellIntensityCurve, this);
                            cell.transform.position = pos;

                            cell.name = count.ToString();
                            _fireGrid.Add(script);
                            count++;
                        }
                    }
                }
            }
        }
    }

    private void SetupAdjacentCells() {
        LayerMask mask = LayerMask.GetMask("TemporaryFireCell");

        for (int i = 0; i < _fireGrid.Count; i++) {
            _fireGrid[i].gameObject.layer = LayerMask.NameToLayer("TemporaryFireCell");
        }

        List<GameObject> gameObjectsToRemove = new List<GameObject>();

        for (int i = _fireGrid.Count - 1; i >= 0; i--) {
            Collider[] colliders = Physics.OverlapSphere(_fireGrid[i].transform.position, CELL_SIZE, mask);
            if (colliders.Length >= 27) {
                gameObjectsToRemove.Add(_fireGrid[i].gameObject);
                _fireGrid.RemoveAt(i);
            }
        }

        for (int i = gameObjectsToRemove.Count - 1; i >= 0; i--) {
            DestroyImmediate(gameObjectsToRemove[i].gameObject);
        }

        for (int i = _fireGrid.Count - 1; i >= 0; i--) {
            List<FireCell> adjacentCells = new List<FireCell>();
            Collider[] colliders = Physics.OverlapSphere(_fireGrid[i].transform.position, CELL_SIZE, mask);
            if (colliders.Length > 0) {
                for (int j = 0; j < colliders.Length; j++) {
                    if (colliders[j].gameObject != _fireGrid[i].gameObject) adjacentCells.Add(colliders[j].GetComponent<FireCell>());
                }
            }
            _fireGrid[i].SetAdjacentCells(new List<FireCell>(adjacentCells));
        }

        for (int i = 0; i < _fireGrid.Count; i++) {
            _fireGrid[i].gameObject.layer = LayerMask.NameToLayer("FireCell");
        }
    }

    private IEnumerator AdjustBurnAmountCoroutine() {
        float currentIntensityValue = 0f;
        float currentBurntValue = 0f;

        float maxIntensityValue = _fireGrid.Count;
        float maxBurntValue = _fireGrid.Count;

        LayerMask mask = LayerMask.GetMask("FireCell");

        while (true) {
            for (int i = 0; i < _fireGrid.Count; i++) {
                currentIntensityValue -= _fireGrid[i].currentIntensity;
                currentBurntValue -= 1f - (_fireGrid[i].currentFuel / _fireGrid[i].maximumFuel);

                if (_fireGrid[i].isBurning) {
                    _fireGrid[i].Burn(Time.deltaTime);
                }

                currentIntensityValue += _fireGrid[i].currentIntensity;
                currentBurntValue += 1f - (_fireGrid[i].currentFuel / _fireGrid[i].maximumFuel);

                _percentageBurnt = currentBurntValue / maxBurntValue;
                _percentageIntensity = currentIntensityValue / maxIntensityValue;

                float burnAmount = Mathf.Lerp(0f, 1.3f, percentageBurnt);
                _meshRenderer.material.SetFloat("_BurnAmount", burnAmount);

                if (!_fireGrid[i].isBurning) continue;
            }

            DamageOtherObjects(currentIntensityValue, mask, Time.deltaTime);

            yield return null;
        }
    }

    private void DamageOtherObjects(float cellIntensity, LayerMask mask, float timePassed) {
        for (int i = 0; i < flames.Count; i++) {
            for (int j = 0; j < flames[i].flameCollider.collidersToDamage.Count; j++) {
                flames[i].flameCollider.collidersToDamage[j].TakeDamage(_fireCellDamageToOtherObjects * cellIntensity * timePassed);
            }
        }
    }

    private float GetPercentageIntensity() {
        float currentIntensityValue = 0f;
        float maxIntensityValue = _fireGrid.Count;

        for (int i = 0; i < _fireGrid.Count; i++) {
            currentIntensityValue += _fireGrid[i].currentIntensity;
        }

        return currentIntensityValue / maxIntensityValue;
    }

    private float GetPercentageBurnt() {
        float currentBurntValue = 0f;
        float maxBurntValue = _fireGrid.Count;

        for (int i = 0; i < _fireGrid.Count; i++) {
            currentBurntValue += (1f - (_fireGrid[i].currentHp / _fireGrid[i].maximumHp));
        }

        return currentBurntValue / maxBurntValue;
    }

    public Flame StartFire(FireCell cell) {
        if (!_isBurning) {
            _adjustBurnAmountCoroutine = StartCoroutine(AdjustBurnAmountCoroutine());
            _isBurning = true;
        }

        Flame newFlame = null;
        if (flames.Count <= 0) {
            GameObject go = GameManager.instance.GetPoolableObject(PoolConstants.fire_particles);
            newFlame = new Flame(PoolManager.instance.SpawnFromPool(go, transform.position, Quaternion.identity), this);
            flames.Add(newFlame);
        } else {
            newFlame = flames[0];
        }
        newFlame.AddCell(cell);

        return newFlame;
    }

    public void RemoveCell(FireCell cell, Flame parentFlame) {
        cell.Kill();

        parentFlame.RemoveCell(cell);

        if (parentFlame.cells.Count <= 0) {
            parentFlame.Destroy();

            if (flames.Count <= 0) {
                _isBurning = false;
                if (_adjustBurnAmountCoroutine != null) {
                    StopCoroutine(_adjustBurnAmountCoroutine);
                    _adjustBurnAmountCoroutine = null;
                }
            }
        }
    }

    private void Burn() {
        for (int i = 0; i < _fireGrid.Count; i++) {
            if (!_fireGrid[i].isBurning) continue;

            _fireGrid[i].Burn(BURN_INTERVAL);
        }
    }
}


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour {

    public class PoolObject {
        public GameObject gameObject;
        public Transform parent;

        public PoolObject(GameObject gameObject, Transform parent) {
            this.gameObject = gameObject;
            this.parent = parent;
        }
    }

    Dictionary<int, Queue<PoolObject>> poolDictionary = new Dictionary<int, Queue<PoolObject>>();
    Dictionary<int, PoolObject> poolObjectsDictionary = new Dictionary<int, PoolObject>();

    private static PoolManager _instance;
	public static PoolManager instance { get => _instance; }

    void Awake() {
        if (_instance == null) {
            _instance = this;
        } else {
            Destroy(gameObject);
        }
    }

	public void CreatePool(GameObject prefab, int poolSize) {
		int poolKey = prefab.GetInstanceID();

        if (!poolDictionary.ContainsKey(poolKey)) {
            poolDictionary.Add(poolKey, new Queue<PoolObject>());

            GameObject poolHolder = new GameObject(prefab.name + " pool");
            poolHolder.transform.SetParent(transform);

            for (int i = 0; i < poolSize; i++) {
                GameObject newObject = Instantiate(prefab);
                PoolObject poolObject = new PoolObject(newObject, poolHolder.transform);
                poolDictionary[poolKey].Enqueue(poolObject);
                newObject.transform.SetParent(poolHolder.transform);
                newObject.SetActive(false);

                poolObjectsDictionary.Add(newObject.GetInstanceID(), poolObject);
            }
        }
	}

	public GameObject SpawnFromPool(GameObject prefab, Vector3 position, Quaternion rotation) {
		int poolKey = prefab.GetInstanceID();

        if (poolDictionary.ContainsKey(poolKey)) {
            PoolObject poolObjectToSpawn = poolDictionary[poolKey].Dequeue();
            GameObject objectToSpawn = poolObjectToSpawn.gameObject;

            objectToSpawn.SetActive(true);
            objectToSpawn.transform.position = position;
            objectToSpawn.transform.rotation = rotation;

            poolDictionary[poolKey].Enqueue(poolObjectToSpawn);

            return objectToSpawn;
        }

        return null;
	}

    public void Destroy(GameObject prefab) {
        prefab.SetActive(false);
        prefab.transform.SetParent(poolObjectsDictionary[prefab.GetInstanceID()].parent, false);
    }

    public void DisableParticles(GameObject prefab) {
        StartCoroutine(DisableParticlesCoroutine(prefab));
    }

    private IEnumerator DisableParticlesCoroutine(GameObject prefab) {
        prefab.GetComponent<ParticleSystem>().Stop();
        prefab.transform.SetParent(poolObjectsDictionary[prefab.GetInstanceID()].parent, true);

        yield return new WaitUntil(() => prefab.GetComponent<ParticleSystem>().particleCount == 0);

        prefab.SetActive(false);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    [System.Serializable]
    public class PoolableObjects {
        public GameObject gameObject;
        public int amount;
    }

    [SerializeField] private List<PoolableObjects> poolableObjects = new List<PoolableObjects>();

    private static GameManager _instance;
    public static GameManager instance { get => _instance; }

    void Awake() {
        if (_instance == null) {
            _instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    void Start() {
        PoolObjects();

        for (int i = 0; i < poolableObjects.Count; i++)
            PoolManager.instance.CreatePool(poolableObjects[i].gameObject, poolableObjects[i].amount);
    }

    private void PoolObjects() {
        for (int i = 0; i < poolableObjects.Count; i++)
            PoolManager.instance.CreatePool(poolableObjects[i].gameObject, poolableObjects[i].amount);
    }

    public GameObject GetPoolableObject(int id) {
        return poolableObjects[id].gameObject;
    }
}

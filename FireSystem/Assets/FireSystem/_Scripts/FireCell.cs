﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCell : MonoBehaviour, ITakeDamage {
    private float _maximumHp;
    public float maximumHp { get => _maximumHp; }
    private float _maximumFuel;
    public float maximumFuel { get => _maximumFuel; }

    private float _currentHp;
    public float currentHp { get => _currentHp; }
    private float _currentFuel;
    public float currentFuel { get => _currentFuel; }

    private float _cellSize;
    private float _damageModifier;
    private AnimationCurve _intensityCurve;

    private float _currentIntensity;
    public float currentIntensity { get => _currentIntensity; }

    private bool _isBurning;
    public bool isBurning { get => _isBurning; }

    private List<FireCell> _adjacentCells = new List<FireCell>();
    public List<FireCell> adjacentCells { get => _adjacentCells; }

    private Flammable _parent = null;

    private Flammable.Flame _parentFlame = null;
    public Flammable.Flame parentFlame { get => _parentFlame; }

    private BoxCollider _boxCollider;
    public BoxCollider boxCollider { get => _boxCollider; }

    public Coroutine damageCoroutine = null;

    void Awake() {
        _boxCollider = GetComponent<BoxCollider>();
    }

    public void SetupObject(float hp, float fuel, float damageModifier, float cellSize, AnimationCurve intensityCurve, Flammable parent) {
        this._maximumHp = hp;
        this._maximumFuel = fuel;
        this._damageModifier = damageModifier;
        this._cellSize = cellSize;
        this._intensityCurve = intensityCurve;
        this._parent = parent;

        this.transform.SetParent(parent.transform, false);

        this.transform.localScale = new Vector3(cellSize, cellSize, cellSize);

        _currentHp = _maximumHp;
        _currentFuel = _maximumFuel;
    }

    public void SetParentFlame(Flammable.Flame parentFlame) {
        this._parentFlame = parentFlame;
    }

    public void SetAdjacentCells(List<FireCell> adjacentCells) {
        _adjacentCells = adjacentCells;
    }

    public void TakeDamage(float damage) {
        if (!_isBurning) {
            _currentHp -= damage;
            if (_currentHp <= 0 && _currentFuel > 0) {
                _currentHp = 0;
                StartBurning();
            }
        }
    }

    private void StartBurning() {
        _isBurning = true;
        _parentFlame = _parent.StartFire(this);
    }

    public void Burn(float burnInterval) {
        _currentFuel -= 1 * burnInterval;
        _currentIntensity = _intensityCurve.Evaluate(_currentFuel / _maximumFuel);

        for (int i = 0; i < _adjacentCells.Count; i++) {
            float damage = _currentIntensity * _damageModifier * burnInterval;
            if (_adjacentCells[i] != null) _adjacentCells[i].TakeDamage(Random.Range(damage / 2, damage));
        }

        if (_currentFuel < 0) {
            _isBurning = false;
            _parent.RemoveCell(this, _parentFlame);
        }
    }

    public void Kill() {
        _parentFlame = null;
    }
}


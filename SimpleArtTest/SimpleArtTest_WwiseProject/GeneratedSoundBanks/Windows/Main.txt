Event	ID	Name			Wwise Object Path	Notes
	293623666	setState			\Default Work Unit\setState	
	454444414	Play_Car_Door_Open			\Default Work Unit\Play_Car_Door_Open	
	543223178	Play_DoorKnock			\Default Work Unit\Play_DoorKnock	
	784600430	Play_CarEngine			\Default Work Unit\Play_CarEngine	
	940963925	Play_OutsideRain			\Default Work Unit\Play_OutsideRain	
	1150137872	Play_door_big_close_001			\Default Work Unit\Play_door_big_close_001	
	1364240704	Play_InsideRain			\Default Work Unit\Play_InsideRain	
	1395412394	Play_DoorCreak			\Default Work Unit\Play_DoorCreak	
	1738890490	Play_Car_Door_Close			\Default Work Unit\Play_Car_Door_Close	
	1866393387	Play_AlleyRain			\Default Work Unit\Play_AlleyRain	
	2142038818	Play_Room_Tone_Small_Sized_Room_2_5_1_LCRLsRsLFE			\Default Work Unit\Play_Room_Tone_Small_Sized_Room_2_5_1_LCRLsRsLFE	
	3141500806	Stop_InsideRain			\Default Work Unit\Stop_InsideRain	
	3815870605	Play_Freezer_Humming_Sound_Effect			\Default Work Unit\Play_Freezer_Humming_Sound_Effect	

State Group	ID	Name			Wwise Object Path	Notes
	271688190	Rain_Ambience			\Default Work Unit\Rain_Ambience	

State	ID	Name	State Group			Notes
	0	None	Rain_Ambience			
	438105790	Outside	Rain_Ambience			
	3553349781	Inside	Rain_Ambience			

Custom State	ID	Name	State Group	Owner		Notes
	264532554	Inside	Rain_Ambience	\Actor-Mixer Hierarchy\Default Work Unit\Ambience\Rain\rain_medium_steady_some_drops_on_metal_h4n		
	522882354	Outside	Rain_Ambience	\Actor-Mixer Hierarchy\Default Work Unit\Ambience\Rain\Heavy rain drops on a car window, steady, recorded indoors, loop		

Game Parameter	ID	Name			Wwise Object Path	Notes
	319209282	RelativeVelocityMagnitude			\Default Work Unit\RelativeVelocityMagnitude	
	2563310688	CarRPM			\Default Work Unit\CarRPM	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	2662870	CAR_Door_Open_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_Door_Open_mono_56D34C19.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CAR_Door_Open_mono		67902
	138315183	OLD STUFF_Doorknob_Creaking_05	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\OLD STUFF_Doorknob_Creaking_05_1DE8BFCF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Objects\Doors\OLD STUFF_Doorknob_Creaking_05		163660
	248238812	CAR_ENGINE_i6_3L_4500_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_4500_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_4500_RPM_loop_mono		208362
	294549041	CAR_ENGINE_i6_3L_600_RPM_idle_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_600_RPM_idle_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_600_RPM_idle_loop_mono		271258
	308040679	CAR_ENGINE_i6_3L_5000_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_5000_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_5000_RPM_loop_mono		291780
	316459651	CAR_ENGINE_i6_3L_5500_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_5500_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_5500_RPM_loop_mono		261548
	407091613	CAR_ENGINE_i6_3L_4000_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_4000_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_4000_RPM_loop_mono		129376
	490574008	Rain On Concrete Water Dripping In Alley	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\Rain On Concrete Water Dripping In Alley_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ambience\Rain\Rain On Concrete Water Dripping In Alley		20178500
	562190423	DoorKnock	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\DOOR KNOCK Bathroom A, Wood, Heavy, Fist, Medium 01, From INSIDE_7723D186.wem		\Actor-Mixer Hierarchy\Default Work Unit\Objects\Doors\DoorKnock		65968
	584237180	Room Tone Small Sized Room 2_5_1 LCRLsRsLFE	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\Room Tone Small Sized Room 2_5.1 LCRLsRsLFE_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ambience\Rooms\Room Tone Small Sized Room 2_5_1 LCRLsRsLFE		69329728
	593829204	Freezer Humming Sound Effect	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\Freezer Humming Sound Effect_2E5CD7B5.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ambience\Rooms\Freezer Humming Sound Effect		1015576
	597114895	CAR_Door_Close_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_Door_Close_mono_56D34C19.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CAR_Door_Close_mono		43728
	602527325	CAR_ENGINE_i6_3L_3500_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_3500_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_3500_RPM_loop_mono		249726
	698600154	Zombie Subject B Hiss (1)	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\Zombie Subject B Hiss (1)_56D34C19.wem		\Actor-Mixer Hierarchy\Default Work Unit\Zombies\Zombie Subject B Hiss (1)		155792
	718743270	CAR_ENGINE_i6_3L_3000_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_3000_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_3000_RPM_loop_mono		310446
	724880034	China Cabinet_Door_Wood with Glass_Close_x5_Fienup_001	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\China Cabinet_Door_Wood with Glass_Close_x5_Fienup_001_56D34C19.wem		\Actor-Mixer Hierarchy\Default Work Unit\Objects\Doors\China Cabinet_Door_Wood with Glass_Close_x5_Fienup_001		2587968
	768436004	Heavy rain drops on a car window, steady, recorded indoors, loop	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\Heavy rain drops on a car window, steady, recorded indoors, loop_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ambience\Rain\Heavy rain drops on a car window, steady, recorded indoors, loop		16896144
	842882147	Mazda_RX7_t12_Var_SFX_Door_Close_Interior_DPA4021	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\Mazda_RX7_t12_Var_SFX_Door_Close_Interior_DPA4021_56D34C19.wem		\Actor-Mixer Hierarchy\Default Work Unit\Objects\Doors\Mazda_RX7_t12_Var_SFX_Door_Close_Interior_DPA4021		164044
	920217273	CAR_ENGINE_i6_3L_1000_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_1000_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_1000_RPM_loop_mono		233896
	958846545	CAR_ENGINE_i6_3L_2500_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_2500_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_2500_RPM_loop_mono		276518
	1003629878	CAR_ENGINE_i6_3L_1500_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_1500_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_1500_RPM_loop_mono		297782
	1017915083	door_big_close_001	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\door_big_close_001_56D34C19.wem		\Actor-Mixer Hierarchy\Default Work Unit\Objects\Doors\door_big_close_001		251728
	1054213617	CAR_ENGINE_i6_3L_2000_RPM_loop_mono	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\CAR_ENGINE_i6_3L_2000_RPM_loop_mono_8956FC97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Car\CarEngine\CAR_ENGINE_i6_3L_2000_RPM_loop_mono		307262
	1056918310	rain_medium_steady_some_drops_on_metal_h4n	C:\UnityProjects\SpookyGame\SimpleArtTest\SimpleArtTest_WwiseProject\.cache\Windows\SFX\rain_medium_steady_some_drops_on_metal_h4n_244B03D3.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ambience\Rain\rain_medium_steady_some_drops_on_metal_h4n		17887820


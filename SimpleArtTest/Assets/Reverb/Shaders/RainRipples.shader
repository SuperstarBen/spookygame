// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Reverb/RainRipples"
{
	Properties
	{
		_BaseTiling("Base Tiling", Float) = 1
		_Albedo("Albedo", 2D) = "white" {}
		_Tint("Tint", Color) = (1,1,1,0)
		_BaseNormal("Base Normal", 2D) = "white" {}
		_RippleNormals("Ripple Normals", 2D) = "bump" {}
		_RippleTiling("Ripple Tiling", Float) = 1
		_Speed("Speed", Float) = 1
		_Wetness("Wetness", Range( 0 , 1)) = 0.5
		_RippleMaskSpeed("Ripple Mask Speed", Float) = 0.4
		_RippleStrength("Ripple Strength", Range( 0 , 1)) = 0.5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _RippleNormals;
		uniform float _RippleStrength;
		uniform float _RippleMaskSpeed;
		uniform float _RippleTiling;
		uniform float _Speed;
		uniform sampler2D _BaseNormal;
		uniform float _BaseTiling;
		uniform sampler2D _Albedo;
		uniform float4 _Tint;
		uniform float _Wetness;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float temp_output_38_0 = ( _RippleMaskSpeed * _Time.y );
			float RippleTime19 = _RippleTiling;
			float2 temp_cast_0 = (RippleTime19).xx;
			float2 uv_TexCoord34 = i.uv_texcoord * temp_cast_0;
			float2 panner35 = ( temp_output_38_0 * float2( 1,1 ) + uv_TexCoord34);
			float simplePerlin2D33 = snoise( panner35 );
			float2 temp_cast_1 = (RippleTime19).xx;
			float2 uv_TexCoord44 = i.uv_texcoord * temp_cast_1;
			float2 panner46 = ( temp_output_38_0 * float2( -1,-1 ) + uv_TexCoord44);
			float simplePerlin2D47 = snoise( panner46 );
			float rippleMask40 = ( _RippleStrength * ( simplePerlin2D33 + simplePerlin2D47 ) );
			float2 temp_cast_2 = (RippleTime19).xx;
			float2 uv_TexCoord2 = i.uv_texcoord * temp_cast_2;
			float2 appendResult13 = (float2(frac( uv_TexCoord2.x ) , frac( uv_TexCoord2.y )));
			float temp_output_4_0_g1 = 4.0;
			float temp_output_5_0_g1 = 4.0;
			float2 appendResult7_g1 = (float2(temp_output_4_0_g1 , temp_output_5_0_g1));
			float totalFrames39_g1 = ( temp_output_4_0_g1 * temp_output_5_0_g1 );
			float2 appendResult8_g1 = (float2(totalFrames39_g1 , temp_output_5_0_g1));
			float temp_output_8_0 = ( _Time.y * _Speed );
			float clampResult42_g1 = clamp( 0.0 , 0.0001 , ( totalFrames39_g1 - 1.0 ) );
			float temp_output_35_0_g1 = frac( ( ( temp_output_8_0 + clampResult42_g1 ) / totalFrames39_g1 ) );
			float2 appendResult29_g1 = (float2(temp_output_35_0_g1 , ( 1.0 - temp_output_35_0_g1 )));
			float2 temp_output_15_0_g1 = ( ( appendResult13 / appendResult7_g1 ) + ( floor( ( appendResult8_g1 * appendResult29_g1 ) ) / appendResult7_g1 ) );
			float2 temp_cast_3 = (( RippleTime19 / 0.6 )).xx;
			float2 uv_TexCoord22 = i.uv_texcoord * temp_cast_3 + float2( 0.77,0.53 );
			float2 appendResult25 = (float2(frac( uv_TexCoord22.x ) , frac( uv_TexCoord22.y )));
			float temp_output_4_0_g2 = 4.0;
			float temp_output_5_0_g2 = 4.0;
			float2 appendResult7_g2 = (float2(temp_output_4_0_g2 , temp_output_5_0_g2));
			float totalFrames39_g2 = ( temp_output_4_0_g2 * temp_output_5_0_g2 );
			float2 appendResult8_g2 = (float2(totalFrames39_g2 , temp_output_5_0_g2));
			float clampResult42_g2 = clamp( 0.0 , 0.0001 , ( totalFrames39_g2 - 1.0 ) );
			float temp_output_35_0_g2 = frac( ( ( temp_output_8_0 + clampResult42_g2 ) / totalFrames39_g2 ) );
			float2 appendResult29_g2 = (float2(temp_output_35_0_g2 , ( 1.0 - temp_output_35_0_g2 )));
			float2 temp_output_15_0_g2 = ( ( appendResult25 / appendResult7_g2 ) + ( floor( ( appendResult8_g2 * appendResult29_g2 ) ) / appendResult7_g2 ) );
			float3 rippleNormals28 = BlendNormals( UnpackScaleNormal( tex2D( _RippleNormals, temp_output_15_0_g1 ), rippleMask40 ) , UnpackScaleNormal( tex2D( _RippleNormals, temp_output_15_0_g2 ), rippleMask40 ) );
			float2 temp_cast_4 = (_BaseTiling).xx;
			float2 uv_TexCoord71 = i.uv_texcoord * temp_cast_4;
			float4 baseNormals65 = tex2D( _BaseNormal, uv_TexCoord71 );
			o.Normal = BlendNormals( rippleNormals28 , baseNormals65 );
			float4 albedo60 = ( tex2D( _Albedo, uv_TexCoord71 ) * _Tint );
			o.Albedo = albedo60.rgb;
			o.Smoothness = _Wetness;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16400
455.5;-1127;1310;1087;-1476.932;1989.053;1.502124;True;True
Node;AmplifyShaderEditor.CommentaryNode;20;-1887.477,-378.7448;Float;False;464.4657;167.2291;Comment;2;3;19;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-1837.477,-326.5157;Float;False;Property;_RippleTiling;Ripple Tiling;5;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19;-1666.011,-328.7448;Float;False;RippleTime;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;41;-538.7212,-1542.353;Float;False;1520.335;869.9022;RippleMask;14;49;47;33;46;35;44;34;38;37;36;39;43;51;50;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleTimeNode;37;-457.6359,-1260.257;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;36;-506.9673,-1480.448;Float;True;19;RippleTime;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;43;-466.1178,-925.9785;Float;True;19;RippleTime;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-487.5513,-1191.322;Float;False;Property;_RippleMaskSpeed;Ripple Mask Speed;8;0;Create;True;0;0;False;0;0.4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-131.794,-1316.413;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;44;-212.4297,-929.6595;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;32;-932.9372,-337.07;Float;False;2425.346;997.9582;Ripple Normals;21;21;31;22;2;7;12;6;23;11;24;13;25;8;9;18;15;5;17;27;28;42;;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;34;-253.2791,-1484.129;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;35;24.36586,-1477.217;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;21;-882.9372,72.1661;Float;False;19;RippleTime;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;46;65.21526,-922.7475;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;47;328.2896,-935.4311;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;33;248.1996,-1492.353;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;31;-675.5822,245.7936;Float;False;2;0;FLOAT;0;False;1;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;22;-655.4557,434.4076;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0.77,0.53;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;51;517.4792,-1465.628;Float;False;Property;_RippleStrength;Ripple Strength;9;0;Create;True;0;0;False;0;0.5;0.46;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-635.6358,-174.7774;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;49;507.2557,-1210.36;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;11;-336.2442,-221.9115;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;23;-366.6307,531.6783;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;12;-346.8106,-77.5066;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;6;-377.3801,186.7033;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;754.3418,-1246.985;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;24;-356.0643,387.2735;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-352.1706,69.23234;Float;False;Property;_Speed;Speed;6;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-123.5073,245.6234;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;13;-184.7953,-204.3011;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;1028.695,-1323.579;Float;True;rippleMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;25;-204.6154,404.8839;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;9;105.626,7.940222;Float;True;Property;_RippleNormals;Ripple Normals;4;0;Create;True;0;0;False;0;None;None;True;bump;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.FunctionNode;15;59.3863,-287.07;Float;False;Flipbook;-1;;1;53c2488c220f6564ca6c90721ee16673;2,71,1,68,0;8;51;SAMPLER2D;0.0;False;13;FLOAT2;0,0;False;4;FLOAT;4;False;5;FLOAT;4;False;24;FLOAT;0;False;2;FLOAT;0;False;55;FLOAT;0;False;70;FLOAT;0;False;5;COLOR;53;FLOAT2;0;FLOAT;47;FLOAT;48;FLOAT;62
Node;AmplifyShaderEditor.RangedFloatNode;70;1367.878,-1395.382;Float;False;Property;_BaseTiling;Base Tiling;0;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;18;103.0132,435.8882;Float;False;Flipbook;-1;;2;53c2488c220f6564ca6c90721ee16673;2,71,1,68,0;8;51;SAMPLER2D;0.0;False;13;FLOAT2;0,0;False;4;FLOAT;4;False;5;FLOAT;4;False;24;FLOAT;0;False;2;FLOAT;0;False;55;FLOAT;0;False;70;FLOAT;0;False;5;COLOR;53;FLOAT2;0;FLOAT;47;FLOAT;48;FLOAT;62
Node;AmplifyShaderEditor.GetLocalVarNode;42;367.7369,73.52646;Float;False;40;rippleMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;63;1870.855,-1603.426;Float;False;1026.38;643.4044;Comment;4;56;52;57;60;;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;71;1558.172,-1425.84;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;66;1878.392,-1922.137;Float;False;652.0237;280;Base Normal;2;65;64;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;17;591.819,213.0641;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;5;594.0577,-128.6345;Float;True;Property;_Tex;Tex;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendNormalsNode;27;1031.621,130.1528;Float;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;64;1928.392,-1872.137;Float;True;Property;_BaseNormal;Base Normal;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;56;2006.295,-1167.021;Float;False;Property;_Tint;Tint;2;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;52;1922.944,-1365.046;Float;True;Property;_Albedo;Albedo;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;2274.402,-1219.592;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;28;1266.909,129.7492;Float;False;rippleNormals;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;65;2287.416,-1830.812;Float;False;baseNormals;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;29;1919.083,-163.5468;Float;False;28;rippleNormals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;69;1925.178,-80.39762;Float;False;65;baseNormals;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;60;2654.235,-1308.966;Float;False;albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;82;3830.156,-1474.383;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;68;2168.295,-137.2105;Float;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;79;3507.264,-1458.27;Float;False;Property;_PuddleStrength;Puddle Strength;10;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;61;2261.598,-474.6577;Float;False;60;albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;10;2299.873,395.3717;Float;False;Property;_Wetness;Wetness;7;0;Create;True;0;0;False;0;0.5;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;75;4179.106,-1488.142;Float;False;PuddleNormals;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2823.397,-160.1081;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Reverb/RainRipples;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;19;0;3;0
WireConnection;38;0;39;0
WireConnection;38;1;37;0
WireConnection;44;0;43;0
WireConnection;34;0;36;0
WireConnection;35;0;34;0
WireConnection;35;1;38;0
WireConnection;46;0;44;0
WireConnection;46;1;38;0
WireConnection;47;0;46;0
WireConnection;33;0;35;0
WireConnection;31;0;21;0
WireConnection;22;0;31;0
WireConnection;2;0;21;0
WireConnection;49;0;33;0
WireConnection;49;1;47;0
WireConnection;11;0;2;1
WireConnection;23;0;22;2
WireConnection;12;0;2;2
WireConnection;50;0;51;0
WireConnection;50;1;49;0
WireConnection;24;0;22;1
WireConnection;8;0;6;0
WireConnection;8;1;7;0
WireConnection;13;0;11;0
WireConnection;13;1;12;0
WireConnection;40;0;50;0
WireConnection;25;0;24;0
WireConnection;25;1;23;0
WireConnection;15;13;13;0
WireConnection;15;2;8;0
WireConnection;18;13;25;0
WireConnection;18;2;8;0
WireConnection;71;0;70;0
WireConnection;17;0;9;0
WireConnection;17;1;18;0
WireConnection;17;5;42;0
WireConnection;5;0;9;0
WireConnection;5;1;15;0
WireConnection;5;5;42;0
WireConnection;27;0;5;0
WireConnection;27;1;17;0
WireConnection;64;1;71;0
WireConnection;52;1;71;0
WireConnection;57;0;52;0
WireConnection;57;1;56;0
WireConnection;28;0;27;0
WireConnection;65;0;64;0
WireConnection;60;0;57;0
WireConnection;82;0;79;0
WireConnection;68;0;29;0
WireConnection;68;1;69;0
WireConnection;0;0;61;0
WireConnection;0;1;68;0
WireConnection;0;4;10;0
ASEEND*/
//CHKSM=CF477FFE70F77A3097D104FE19445FF754F4E006
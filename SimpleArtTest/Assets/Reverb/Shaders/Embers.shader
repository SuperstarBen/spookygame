// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Embers"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_Charcoal("Charcoal", 2D) = "white" {}
		_CharAmount("Char Amount", Range( 0 , 1.5)) = 0
		_CharMask("CharMask", 2D) = "white" {}
		_TextureSample4("Texture Sample 4", 2D) = "bump" {}
		_HeatWave("Heat Wave", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _Charcoal;
		uniform float4 _Charcoal_ST;
		uniform sampler2D _CharMask;
		uniform sampler2D _TextureSample4;
		uniform float _HeatWave;
		uniform float _CharAmount;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float2 uv_Charcoal = i.uv_texcoord * _Charcoal_ST.xy + _Charcoal_ST.zw;
			float2 panner29 = ( 1.0 * _Time.y * float2( 0,-1 ) + i.uv_texcoord);
			float4 tex2DNode10 = tex2D( _CharMask, ( ( (UnpackNormal( tex2D( _TextureSample4, panner29 ) )).xy * _HeatWave ) + i.uv_texcoord ) );
			float temp_output_12_0 = step( tex2DNode10.r , _CharAmount );
			float4 lerpResult27 = lerp( tex2D( _Albedo, uv_Albedo ) , ( tex2D( _Charcoal, uv_Charcoal ) * temp_output_12_0 ) , _CharAmount);
			o.Albedo = lerpResult27.rgb;
			float4 color23 = IsGammaSpace() ? float4(2,1.733333,0,0) : float4(4.594794,3.353827,0,0);
			o.Emission = ( color23 * ( temp_output_12_0 - step( tex2DNode10.r , ( _CharAmount / 1.2 ) ) ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16400
294;-1007;977;967;2128.765;529.9636;1.755729;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;28;-2727.286,140.145;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;29;-2416.555,152.217;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;30;-2055.4,214.9339;Float;True;Property;_TextureSample4;Texture Sample 4;4;0;Create;True;0;0;False;0;None;2d236a12c4556784fa64eb7a306588c1;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;32;-1689.914,430.7823;Float;False;Property;_HeatWave;Heat Wave;5;0;Create;True;0;0;False;0;0;0.82;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;31;-1656.992,207.769;Float;True;True;True;False;True;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-1369.083,271.585;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;34;-1472.424,555.0944;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;20;-336.0001,707.5518;Float;False;Constant;_Float0;Float 0;4;0;Create;True;0;0;False;0;1.2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;35;-1211.637,389.2951;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;11;-1079.911,207.2088;Float;True;Property;_CharMask;CharMask;3;0;Create;True;0;0;False;0;None;d3740ea3b09c73e4da785e99bafef8f3;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-717.9976,519.7391;Float;False;Property;_CharAmount;Char Amount;2;0;Create;True;0;0;False;0;0;1;0;1.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;19;-202.4776,585.7674;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;10;-826.0721,233.6198;Float;True;Property;_TextureSample2;Texture Sample 2;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;3;-1041.761,-231.5079;Float;True;Property;_Charcoal;Charcoal;1;0;Create;True;0;0;False;0;None;bcefb4754815f7243a605dc6b4412c7f;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.StepOpNode;18;-39.60968,177.8641;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;12;-440.1771,217.4797;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;4;-743.9038,-188.9568;Float;True;Property;_TextureSample1;Texture Sample 1;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;2;-805.529,-482.4126;Float;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;False;0;None;662d72b6ec210cf4cbeec2b4d3cb8b2a;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.ColorNode;23;63.10025,629.786;Float;False;Constant;_Color0;Color 0;4;1;[HDR];Create;True;0;0;False;0;2,1.733333,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;24;230.3711,268.8352;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;16;-336.6339,-57.56833;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;1;-432.8401,-398.7779;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;27;-29.3645,-151.0844;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;553.1714,343.6663;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;221,-311;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Embers;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;29;0;28;0
WireConnection;30;1;29;0
WireConnection;31;0;30;0
WireConnection;33;0;31;0
WireConnection;33;1;32;0
WireConnection;35;0;33;0
WireConnection;35;1;34;0
WireConnection;19;0;6;0
WireConnection;19;1;20;0
WireConnection;10;0;11;0
WireConnection;10;1;35;0
WireConnection;18;0;10;1
WireConnection;18;1;19;0
WireConnection;12;0;10;1
WireConnection;12;1;6;0
WireConnection;4;0;3;0
WireConnection;24;0;12;0
WireConnection;24;1;18;0
WireConnection;16;0;4;0
WireConnection;16;1;12;0
WireConnection;1;0;2;0
WireConnection;27;0;1;0
WireConnection;27;1;16;0
WireConnection;27;2;6;0
WireConnection;21;0;23;0
WireConnection;21;1;24;0
WireConnection;0;0;27;0
WireConnection;0;2;21;0
ASEEND*/
//CHKSM=87798B763E3F6EF9264B29A5FF4021FA36EB02AF
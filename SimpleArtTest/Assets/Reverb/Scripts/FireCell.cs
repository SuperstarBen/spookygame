﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCell : MonoBehaviour, ITakeDamage
{
    [SerializeField]
    private float hp = 1;
    //private float currentHp;
    [SerializeField]
    private float fuel = 20;
    private GameObject firePfx;
    private float currentFuel;
    private bool burning;
    [SerializeField]
    private float fuelUpdateInterval = 1;
    private float burnTime;
    [SerializeField]
    private string burnSFX;
    [SerializeField]
    private AnimationCurve intensityCurve;
    [SerializeField]
    private float intensityModifier;
    [SerializeField]
    private float sphereRadius;
    public List<FireCell> adjacentCells;

    private Flammable parentFlammable;
    private BoxCollider col;

    private Coroutine depleteFuel;

    public float Hp { get => hp; set => hp = value; }
    public float Fuel { get => fuel; set => fuel = value; }
    public BoxCollider Col { get => col; set => col = value; }
    public float SphereRadius { get => sphereRadius; set => sphereRadius = value; }
    public Flammable ParentFlammable { get => parentFlammable; set => parentFlammable = value; }

    private void Awake()
    {
        currentFuel = Fuel;
        col = GetComponent<BoxCollider>();
    }

    public void TakeDamage(float damage)
    {
        if (!burning)
        {
            hp -= damage;
            if (hp <= 0 && currentFuel > 0)
            {
                StartBurning();
            }
        }
    }

    public void StartBurning()
    {

        AudioManager.instance.PlaySound(burnSFX);
        firePfx = ObjectPooler.instance.GetPooledObject();
        firePfx.transform.position = this.transform.position;
        firePfx.transform.rotation = this.transform.rotation;
        firePfx.transform.SetParent(transform);
        firePfx.SetActive(true);
        burning = true;
        if (depleteFuel == null) depleteFuel = StartCoroutine(Burn());

    }

    IEnumerator Burn()
    {
        while (burning && currentFuel > 0)
        {
            yield return Yielders.Get(fuelUpdateInterval);
            currentFuel -= 1;
            float currentIntensity = intensityCurve.Evaluate(currentFuel / Fuel);
            ParticleSystem pfx = firePfx.GetComponentInChildren<ParticleSystem>();
            ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();
            emitParams.startSize = currentIntensity;
            pfx.Emit(emitParams,1);
              
           
            //Damage surrounding objects (including other firecells)
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, sphereRadius);
            foreach (Collider col in hitColliders)
            {
                ITakeDamage td = col.GetComponent<ITakeDamage>();

                if (td != null)
                {
                    td.TakeDamage(currentIntensity * intensityModifier);
                }

            }

        }
        burning = false;
        firePfx.SetActive(false);
        firePfx.transform.SetParent(null);
        firePfx = null;
        if(burnSFX != null) AudioManager.instance.StopSound(burnSFX);
    }

    private bool allAdjCellsOnFire()
    {
        foreach(FireCell adjCell in adjacentCells)
        {
            if(!adjCell.burning)
            {
                return false;
            }
        }
        return true;
    }
}


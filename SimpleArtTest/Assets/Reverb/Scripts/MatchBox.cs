﻿
using UnityEngine;

public class MatchBox : Item, IUseItem
{
    [SerializeField]
    private GameObject match;
    private int matchCapacity = 20;
    private int matchCount;
    private GameObject currentMatch;
    private Transform hand;

    private void Awake()
    {
        matchCount = matchCapacity;
    }

    private void Start()
    {
        hand = transform.parent;
    }


    public void UseItem()
    {
        if(matchCount > 0)
        {
            if(currentMatch != null)
            {
                currentMatch.GetComponent<Rigidbody>().isKinematic = false;
                currentMatch.transform.SetParent(null);

            }
            
            currentMatch = Instantiate(match, transform.position, transform.rotation);
            currentMatch.transform.SetParent(hand);
            currentMatch.GetComponent<ITakeDamage>().TakeDamage(20f);
            matchCount -= 1;
        }

    }
}

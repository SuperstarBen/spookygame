﻿using UnityEngine;
using InControl;

public class PlayerCharacterActions : PlayerActionSet
{
        public PlayerAction useRightHand;
        public PlayerAction useLeftHand;
        public PlayerAction moveLeft;
        public PlayerAction moveRight;
        public PlayerAction moveUp;
        public PlayerAction moveDown;
        public PlayerTwoAxisAction move;
        public PlayerAction lookLeft;
        public PlayerAction lookRight;
        public PlayerAction lookUp;
        public PlayerAction lookDown;
        public PlayerTwoAxisAction look;
    public PlayerAction action;
    public PlayerAction enterCar;
    public PlayerAction reload;


        public PlayerCharacterActions()
        {
            useRightHand = CreatePlayerAction("useRightHand");
            useLeftHand = CreatePlayerAction("useLeftHand");
            moveLeft = CreatePlayerAction("Move Left");
            moveRight = CreatePlayerAction("Move Right");
            moveUp = CreatePlayerAction("Move Up");
            moveDown = CreatePlayerAction("Move Down");
            move = CreateTwoAxisPlayerAction(moveLeft, moveRight, moveDown, moveUp);

            lookLeft = CreatePlayerAction("Look Left");
            lookRight = CreatePlayerAction("Look Right");
            lookUp = CreatePlayerAction("Look Up");
            lookDown = CreatePlayerAction("Look Down");
            action = CreatePlayerAction("Action");
            enterCar = CreatePlayerAction("Enter Car");
            reload = CreatePlayerAction("Reload");
            look = CreateTwoAxisPlayerAction(lookLeft, lookRight, lookUp, lookDown);

        }


        public static PlayerCharacterActions CreateWithDefaultBindings()
        {
            var playerActions = new PlayerCharacterActions();

        #region useControls
        playerActions.useRightHand.AddDefaultBinding(InputControlType.RightTrigger);
        playerActions.useRightHand.AddDefaultBinding(Mouse.RightButton);

        playerActions.useLeftHand.AddDefaultBinding(Mouse.LeftButton);
        playerActions.useLeftHand.AddDefaultBinding(InputControlType.LeftTrigger);

        playerActions.action.AddDefaultBinding(InputControlType.Action1);
        playerActions.enterCar.AddDefaultBinding(InputControlType.Action2);
        playerActions.reload.AddDefaultBinding(InputControlType.Action3);
        playerActions.action.AddDefaultBinding(Key.Space);
        playerActions.enterCar.AddDefaultBinding(Key.Control);
        playerActions.reload.AddDefaultBinding(Key.R);
        #endregion

        #region moveControls
        playerActions.moveLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
            playerActions.moveRight.AddDefaultBinding(InputControlType.LeftStickRight);
            playerActions.moveUp.AddDefaultBinding(InputControlType.LeftStickUp);
            playerActions.moveDown.AddDefaultBinding(InputControlType.LeftStickDown);

            playerActions.moveLeft.AddDefaultBinding(Key.A);
            playerActions.moveRight.AddDefaultBinding(Key.D);
            playerActions.moveUp.AddDefaultBinding(Key.W);
            playerActions.moveDown.AddDefaultBinding(Key.S);
        #endregion

        #region lookControls

        playerActions.lookLeft.AddDefaultBinding(InputControlType.RightStickLeft);
        playerActions.lookRight.AddDefaultBinding(InputControlType.RightStickRight);
        playerActions.lookDown.AddDefaultBinding(InputControlType.RightStickDown);
        playerActions.lookUp.AddDefaultBinding(InputControlType.RightStickUp);
        #endregion

        #region ListenOptions

        playerActions.ListenOptions.IncludeUnknownControllers = true;
            playerActions.ListenOptions.MaxAllowedBindings = 1;
            playerActions.ListenOptions.UnsetDuplicateBindingsOnSet = true;
            playerActions.ListenOptions.IncludeMouseButtons = true;
            playerActions.ListenOptions.IncludeMouseScrollWheel = true;
            playerActions.ListenOptions.OnBindingFound = (action, binding) => {
                if (binding == new KeyBindingSource(Key.Escape))
                {
                    action.StopListeningForBinding();
                    return false;
                }
                return true;
            };

            playerActions.ListenOptions.OnBindingAdded += (action, binding) => {
                Debug.Log("Binding added... " + binding.DeviceName + ": " + binding.Name);
            };

            playerActions.ListenOptions.OnBindingRejected += (action, binding, reason) => {
                Debug.Log("Binding rejected... " + reason);
            };
        #endregion
        return playerActions;
        }
    }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Item, IUseItem, IReload
{
    public float damage;
    public float fireRate;
    public float reloadTime;
    public float recoilStrength;
    public GameObject casingPrefab;
    public GameObject muzzleFlashPrefab;
    public Transform barrelLocation;
    public Transform casingExitLocation;
    public Magazine loadedMagazine;
    public GameObject MagazinePrefab;
    public Transform MagazineSpawn;

    public void UseItem()
    {
        if(GunIsLoaded())
        {
            FireGun();
        }
        else
        {
            DryFire();
        }
    }

    public bool GunIsLoaded()
    {
        if (loadedMagazine != null && loadedMagazine.BulletsHeld > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }

    public void FireGun()
    {

        GetComponent<Animator>().SetTrigger("Fire");
        AudioManager.instance.PlaySound("fire");
        muzzleFlashPrefab.SetActive(true);
        loadedMagazine.BulletsHeld -= 1;
        FireBullet();
    }

    public void DryFire()
    {
        AudioManager.instance.PlaySound("dryFire");

    }


    void CasingRelease()
    {
        GameObject casing;
        casing = Instantiate(casingPrefab, casingExitLocation.position, casingExitLocation.rotation) as GameObject;
        casing.GetComponent<Rigidbody>().AddExplosionForce(550f, (casingExitLocation.position - casingExitLocation.right * 0.3f - casingExitLocation.up * 0.6f), 1f);
        casing.GetComponent<Rigidbody>().AddTorque(new Vector3(0, Random.Range(100f, 500f), Random.Range(10f, 1000f)), ForceMode.Impulse);
    }

    public void Reload()
    {
        Rigidbody magPhys = loadedMagazine.GetComponent<Rigidbody>();
        loadedMagazine.transform.parent = null;
        magPhys.useGravity = true;
        AudioManager.instance.PlaySound("removeMag");
        magPhys.isKinematic = false;
        magPhys.AddTorque(new Vector3(Random.Range(1f, 2f),0 , 0), ForceMode.Impulse);
      
        GameObject newMagazine = Instantiate(MagazinePrefab, MagazineSpawn.position, MagazineSpawn.rotation);
        newMagazine.transform.SetParent(MagazineSpawn);
        loadedMagazine = newMagazine.GetComponent<Magazine>();

    }

    void FireBullet()
    {
        RaycastHit gunHit;
        Ray ray = new Ray(barrelLocation.transform.position,barrelLocation.transform.forward * 100);
        Debug.DrawRay(barrelLocation.transform.position, barrelLocation.transform.forward * 100);
        

        if (Physics.Raycast(ray, out gunHit) && gunHit.collider.GetComponent<MaterialProperties>() != null)
        {
            
            MaterialProperties matProps = gunHit.collider.GetComponent<MaterialProperties>();
            AudioManager.instance.PlaySound(matProps.hitSfx);
            Quaternion quat = Quaternion.FromToRotation(Vector3.forward, gunHit.normal);
            GameObject pfx = Instantiate(matProps.pfx, gunHit.point, quat);
            //matProps.transform.parent = gunHit.collider.transform;
            pfx.transform.SetParent(gunHit.collider.transform);
            if(gunHit.collider.GetComponent<Rigidbody>())
            {
                gunHit.collider.GetComponent<Rigidbody>().AddForceAtPosition(barrelLocation.transform.forward *1000, gunHit.point);
            }
            
            
        }
    }


}

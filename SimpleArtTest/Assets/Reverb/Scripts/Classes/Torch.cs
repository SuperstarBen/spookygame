﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : Item, IUseItem
{
    private bool on = false;
    private Battery battery;
    public GameObject light;

    public void UseItem()
    {
        AudioManager.instance.PlaySound("torchClick");

        if (on)
        {
            light.SetActive(false);
            on = false;
        }
        else
        {
            light.SetActive(true);
            on = true;
        }


    }

}

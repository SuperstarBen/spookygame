﻿
using UnityEngine;
using InControl;


public class CarActions : PlayerActionSet
{
    public PlayerAction accelerate;
    public PlayerAction brake;
    public PlayerAction steerLeft;
    public PlayerAction steerRight;
    public PlayerAction handBrake;
    public PlayerAction openBoot;
    public PlayerAction openLeftDoor;
    public PlayerAction openRightDoor;
    public PlayerTwoAxisAction steer;

    public CarActions()
    {
        accelerate = CreatePlayerAction("accellerate");
        brake = CreatePlayerAction("brake");
        steerLeft = CreatePlayerAction("steer Left");
        steerRight = CreatePlayerAction("steerRight");
        handBrake = CreatePlayerAction("HandBrake");
        steer = CreateTwoAxisPlayerAction(steerLeft, steerRight, brake, accelerate);
    }

    public static CarActions CreateWithDefaultBindings()
    {
        var carActions = new CarActions();

        #region useControls
        carActions.accelerate.AddDefaultBinding(InputControlType.RightTrigger);
        carActions.accelerate.AddDefaultBinding(Key.W);

        carActions.brake.AddDefaultBinding(InputControlType.LeftTrigger);
        carActions.brake.AddDefaultBinding(Key.S);
        #endregion

        #region steer
        carActions.steerLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
        carActions.steerRight.AddDefaultBinding(InputControlType.LeftStickRight);
        
        carActions.steerLeft.AddDefaultBinding(Key.A);
        carActions.steerRight.AddDefaultBinding(Key.D);

        carActions.handBrake.AddDefaultBinding(InputControlType.Action1);
        carActions.handBrake.AddDefaultBinding(Key.Space);
        #endregion

        #region ListenOptions

        carActions.ListenOptions.IncludeUnknownControllers = true;
        carActions.ListenOptions.MaxAllowedBindings = 1;
        carActions.ListenOptions.UnsetDuplicateBindingsOnSet = true;
        carActions.ListenOptions.IncludeMouseButtons = true;
        carActions.ListenOptions.IncludeMouseScrollWheel = true;
        carActions.ListenOptions.OnBindingFound = (action, binding) => {
            if (binding == new KeyBindingSource(Key.Escape))
            {
                action.StopListeningForBinding();
                return false;
            }
            return true;
        };

        carActions.ListenOptions.OnBindingAdded += (action, binding) => {
            Debug.Log("Binding added... " + binding.DeviceName + ": " + binding.Name);
        };

        carActions.ListenOptions.OnBindingRejected += (action, binding, reason) => {
            Debug.Log("Binding rejected... " + reason);
        };
        #endregion
        return carActions;
    }
}

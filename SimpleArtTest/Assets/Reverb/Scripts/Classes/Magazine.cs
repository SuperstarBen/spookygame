﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Magazine : Item
{
    public int _magazineType;
    public int _capacity;
    public int _bulletsHeld;


    public int BulletsHeld { get => _bulletsHeld; set => _bulletsHeld = value; }

    public Magazine()
    {}

    public Magazine(int magazineType, int capacity, int bulletsHeld)
    {
        _magazineType = magazineType;
        _capacity = capacity;
        _bulletsHeld = bulletsHeld;
    }

    public void DecrementBullets()
    {
        _bulletsHeld -= 1;
    }
}

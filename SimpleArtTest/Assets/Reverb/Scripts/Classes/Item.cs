﻿
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField]
    private string itemName;
    [SerializeField]
    private bool canPickup;
    [SerializeField]
    private bool twoHanded;
    private bool isBeingHeld;

    public bool IsBeingHeld { get => isBeingHeld; set => isBeingHeld = value; }
    public bool CanPickup { get => canPickup; set => canPickup = value; }
    public bool TwoHanded { get => twoHanded; set => twoHanded = value; }
    public string ItemName { get => itemName; set => itemName = value; }
}

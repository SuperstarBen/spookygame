﻿using UnityEngine;
using InControl;

public class TestPlayerActions : PlayerActionSet
{
    public PlayerAction useRightHand;
    


    public TestPlayerActions()
    {
        useRightHand = CreatePlayerAction("useRightHand");
       
    }


    public static TestPlayerActions CreateWithDefaultBindings()
    {
        var playerActions = new TestPlayerActions();

        playerActions.useRightHand.AddDefaultBinding(InputControlType.RightTrigger);
        playerActions.useRightHand.AddDefaultBinding(Mouse.RightButton);


        playerActions.ListenOptions.OnBindingFound = (action, binding) => {
            if (binding == new KeyBindingSource(Key.Escape))
            {
                action.StopListeningForBinding();
                return false;
            }
            return true;
        };

        playerActions.ListenOptions.OnBindingAdded += (action, binding) => {
            Debug.Log("Binding added... " + binding.DeviceName + ": " + binding.Name);
        };

        playerActions.ListenOptions.OnBindingRejected += (action, binding, reason) => {
            Debug.Log("Binding rejected... " + reason);
        };

        return playerActions;
    }
}


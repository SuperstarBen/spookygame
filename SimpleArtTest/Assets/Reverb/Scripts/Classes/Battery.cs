﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : Item
{
    public int batteryType;
    public float powerCapacity;
    public float powerRemaining;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    private bool isHoldingItem;
    public Item heldItem;

    public bool IsHoldingItem { get => isHoldingItem; set => isHoldingItem = value; }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerController : Controller
{
    public float movementSpeed = 5;
    public Dictionary<int, int> magazineInv;
    [SerializeField]
    private float throwForce;

    [SerializeField]
    private Hand leftHand;
    [SerializeField]
    private Hand rightHand;
    private Camera camera;
    private PlayerCharacterActions characterActions;
    private GameObject lookTarget;


    private void OnEnable()
    {
        characterActions = PlayerCharacterActions.CreateWithDefaultBindings();
    }

    private void OnDisable()
    {
        characterActions.Destroy();

    }

    private void Start()
    {
        camera = Camera.main;

    }

    private void Update()
    {
        HandleMovementInput();
        HandleLookInput();
        // HandleReloadGun();
        UseLeftHandItem();
        UseRightHandItem();
        //HandlePickUpItem();
    }

    private void HandlePickUpItem()
    {
        if (Input.GetKeyDown("q"))
        {
            PickUpItem(rightHand);
        }
        if (Input.GetKeyDown("e"))
        {
            PickUpItem(leftHand);
        }
    }

    //Casts a sphere near the player to see if there are any items nearby. If there are then it picks them up.
    private void PickUpItem(Hand hand)
    {

        Collider[] items = Physics.OverlapSphere(transform.position, 2);
        int i = 0;
        while (i < items.Length)
        {
            Item currentItem = items[i].GetComponent<Item>();
            if (currentItem != null && currentItem.CanPickup == true && currentItem.IsBeingHeld == false)
            {

                if (hand.IsHoldingItem == true)
                {
                    Rigidbody rb = hand.heldItem.GetComponent<Rigidbody>();
                    hand.heldItem.transform.SetParent(null);
                    rb.isKinematic = false;
                    rb.AddForce(hand.transform.forward * throwForce);
                    hand.heldItem.IsBeingHeld = false;

                }
                hand.heldItem = currentItem;
                hand.heldItem.IsBeingHeld = true;
                hand.IsHoldingItem = true;
                hand.heldItem.transform.SetParent(hand.transform);
                hand.heldItem.GetComponent<Rigidbody>().isKinematic = true;
                hand.heldItem.transform.localPosition = Vector3.zero;
                hand.heldItem.transform.localRotation = Quaternion.identity;
                break;
            }
            i++;
        }

    }

    void HandleMovementInput()
    {
       
        Vector3 forward = camera.transform.TransformDirection(Vector3.forward);
        forward.y = 0;
        forward = forward.normalized;
        Vector3 right = new Vector3(forward.z, 0, -forward.x);

        float _horizontal = characterActions.move.X;
        float _vertical = characterActions.move.Y;

        Vector3 moveDirection = (_horizontal * right + _vertical * forward);
        transform.Translate(moveDirection * movementSpeed * Time.deltaTime, Space.World);


    }

    void HandleLookInput()
    {
        // Look Controls if you are using the Mouse
        if (characterActions.LastInputType == BindingSourceType.MouseBindingSource || characterActions.LastInputType == BindingSourceType.KeyBindingSource)
        {
            RaycastHit _hit;
            Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(_ray, out _hit) && _hit.collider.gameObject.layer != 10)
            {
                transform.LookAt(new Vector3(_hit.point.x, transform.position.y, _hit.point.z));
            }

        }

        // Look Controls if you are using the controller
        if (characterActions.LastInputType == BindingSourceType.DeviceBindingSource)
        {
            Vector3 lookTargetPos = new Vector3(-characterActions.look.Y + transform.position.x, transform.position.y, -characterActions.look.X + transform.position.z);
            transform.LookAt(lookTargetPos);



            //Insert Controller Look Controls here

        }


    }

    void UseRightHandItem()
    {
        if (characterActions.useRightHand)
        {
            if (rightHand.IsHoldingItem)
            {
                IUseItem usable = rightHand.heldItem.GetComponent<IUseItem>();
                if (usable != null)
                {
                    usable.UseItem();
                }
            }

        }
    }

    void UseLeftHandItem()
    {
        if (characterActions.useLeftHand)
        {
            if (leftHand.IsHoldingItem)
            {
                IUseItem usable = leftHand.heldItem.GetComponent<IUseItem>();
                if (usable != null)
                {
                    usable.UseItem();
                }
            }

        }
    }


    void HandleReload()
    {
        if (characterActions.reload)
        {
            if (leftHand.heldItem != null)
            {
                IReload reloadable = leftHand.heldItem.GetComponent<IReload>();
                if (reloadable != null)
                    reloadable.Reload();
            }

            if (rightHand.heldItem != null)
            {
                IReload reloadable = rightHand.heldItem.GetComponent<IReload>();
                if (reloadable != null)
                    reloadable.Reload();
            }
        }
    }
}

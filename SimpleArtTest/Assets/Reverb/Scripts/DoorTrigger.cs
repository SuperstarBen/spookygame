﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public bool playerByDoor;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerByDoor = true;
        }
    }


    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player")
        {
            playerByDoor = false;
        }
    }

}

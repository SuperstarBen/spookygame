﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionAudio : MonoBehaviour
{

    public string collisionSound;


    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 2)
        {
            AudioManager.instance.PlaySound(collisionSound);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBoot : Door
{
    private VehicleManager vehicle;
    [SerializeField]
    private GameObject camera;
    [SerializeField]
    private GameObject inventory;
   
    

    private void OnEnable()
    {
        CharacterActions = PlayerCharacterActions.CreateWithDefaultBindings();
    }
    // Start is called before the first frame update
    void Start()
    {
        camera.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Trigger.playerByDoor && CharacterActions.action.WasPressed)
        {
            DoorToggle();
            inventory.SetActive(true);
            camera.SetActive(true);
            FindObjectOfType<PlayerController>().gameObject.SetActive(false);
        }
    }
}

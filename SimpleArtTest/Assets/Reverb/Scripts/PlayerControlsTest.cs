﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerControlsTest : MonoBehaviour
{
    TestPlayerActions playerActions;

    void OnEnable()
    {
        // See PlayerActions.cs for this setup.
        playerActions = TestPlayerActions.CreateWithDefaultBindings();
   
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(playerActions.useRightHand.Value);
        
    }
}

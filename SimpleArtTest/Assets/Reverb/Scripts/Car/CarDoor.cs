﻿using UnityEngine;
using System.Collections;

public class CarDoor : Door
{
    [SerializeField]
    private VehicleManager vehicle;

    private void OnEnable()
    {
        CharacterActions = PlayerCharacterActions.CreateWithDefaultBindings();
    }


    private void Update()
    {
        if (Trigger.playerByDoor && CharacterActions.action.WasPressed)
        {
            DoorToggle();
        }
        if(Trigger.playerByDoor && doorOpen & CharacterActions.enterCar)
        {
            vehicle.playerInControl = true;
            DoorToggle();
            Destroy(FindObjectOfType<PlayerController>().gameObject);

        }
    }
}

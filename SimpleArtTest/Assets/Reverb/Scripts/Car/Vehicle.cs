﻿using UnityEngine;

namespace Adrenak.Tork {
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CenterOfMassAssigner))]
	[RequireComponent(typeof(AntiRoll))]
	[RequireComponent(typeof(Aerodynamics))]
	[RequireComponent(typeof(Motor))]
	[RequireComponent(typeof(Steering))]
	[RequireComponent(typeof(Brakes))]
    [RequireComponent(typeof(VehicleManager))]
	public class Vehicle : MonoBehaviour {
		[SerializeField] Rigidbody m_Rigidbody;
		[SerializeField] Steering m_Steering;
		[SerializeField] Motor m_Motor;
		[SerializeField] Brakes m_Brake;
		[SerializeField] Aerodynamics m_Aerodynamics;
		[SerializeField] VehicleManager m_Manager;
        [SerializeField] float speed;

        [SerializeField] float m_MaxReverseInput = -.5f;

		[Tooltip("The maximum motor torque available based on the speed (KMPH)")]
		[SerializeField] AnimationCurve m_MotorTorqueVsSpeed = AnimationCurve.Linear(0, 10000, 250, 0);

		[Tooltip("The steering angle based on the speed (KMPH)")]
		[SerializeField] AnimationCurve m_MaxSteeringAngleVsSpeed = AnimationCurve.Linear(0, 35, 250, 5);

		[Tooltip("The down force based on the speed (KMPH)")]
		[SerializeField] AnimationCurve m_DownForceVsSpeed = AnimationCurve.Linear(0, 0, 250, 2500);

        private CarActions carActions;
        private void OnEnable()

        {
            carActions = CarActions.CreateWithDefaultBindings();
        }
         

        private void Update() {
            float rpm = speed;
            AkSoundEngine.SetRTPCValue("CarRPM", rpm);
            if (m_Manager.playerInControl)
            {

                speed = Vector3.Dot(m_Rigidbody.velocity, transform.forward) * 3.6F;

                m_Steering.range = m_MaxSteeringAngleVsSpeed.Evaluate(speed);
                m_Steering.value = carActions.steer.X;

                m_Motor.maxTorque = m_MotorTorqueVsSpeed.Evaluate(speed);
                m_Motor.value = Mathf.Clamp(-carActions.brake + carActions.accelerate, m_MaxReverseInput, 1);

                m_Aerodynamics.downForce = m_DownForceVsSpeed.Evaluate(speed);
                m_Aerodynamics.midAirSteerInput = carActions.steer.X;
                m_Brake.value = carActions.brake;

                if (speed <= 0f)
                {
                    m_Brake.value = 0;
                }
            }

            

			
		}
	}
}

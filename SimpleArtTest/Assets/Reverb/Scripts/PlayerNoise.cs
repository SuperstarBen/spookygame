﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNoise : MonoBehaviour
{
    private Collider playerCol;
    public SphereCollider noiseCol;

    // Start is called before the first frame update
    void Start()
    {
        playerCol = GetComponent<Collider>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        noiseCol.radius = collision.relativeVelocity.magnitude;
    }
}

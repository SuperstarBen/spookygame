﻿

using System.Collections.Generic;
using UnityEngine;

public class Flammable : MonoBehaviour
{
    private Dictionary<Vector3, FireCell > fireGrid = new Dictionary<Vector3, FireCell>();
    public FireCell fireCell;
    [SerializeField]
    private float cellSize = 0.1f;
    private Renderer mat;
    private float totalFuel;

    void Start()
    {
        Collider col = GetComponent<Collider>();
        mat = GetComponent<Renderer>();
        Bounds meshBounds = col.bounds;
        Vector3 min = meshBounds.min;
        Vector3 max = meshBounds.max;

        for (float x = min.x; x <= max.x; x = x + cellSize)
        {
            for (float y = min.y; y <= max.y; y = y + cellSize)
            {
                for (float z = min.z; z <= max.z; z = z + cellSize)
                {
                    Vector3 pos = new Vector3(x, y, z) + new Vector3(cellSize/2, cellSize / 2, cellSize / 2);

                    Collider[] colliders = Physics.OverlapSphere(pos, .2f);
                    if (colliders.Length > 0)
                    {
                        FireCell cell = (FireCell)Instantiate(fireCell, pos, Quaternion.identity);
                        cell.transform.SetParent(transform);
                        fireGrid.Add(pos, cell);
                        cell.Col.size = new Vector3(cellSize, cellSize, cellSize);
                        cell.SphereRadius = cellSize * 1.1f;
                        cell.ParentFlammable = this;

                    }
                }
            }
        }
        FindAdjacentCells();
    }

    private void FindAdjacentCells()
    {
        foreach(KeyValuePair<Vector3, FireCell> posCell in fireGrid)
        {
            FireCell cell = posCell.Value;
            Vector3 pos = posCell.Key;
            if(fireGrid.ContainsKey(pos + new Vector3(-cellSize, 0, 0))) cell.adjacentCells.Add(fireGrid[pos + new Vector3(-cellSize, 0, 0)]);
            if(fireGrid.ContainsKey(pos + new Vector3(cellSize, 0, 0))) cell.adjacentCells.Add(fireGrid[pos + new Vector3(cellSize, 0, 0)]);

            if(fireGrid.ContainsKey(pos + new Vector3(0, -cellSize, 0))) cell.adjacentCells.Add(fireGrid[pos + new Vector3(0,-cellSize, 0)]);
            if(fireGrid.ContainsKey(pos + new Vector3(0, cellSize, 0))) cell.adjacentCells.Add(fireGrid[pos + new Vector3(0,cellSize, 0)]);
            
            if(fireGrid.ContainsKey(pos + new Vector3(0, 0, -cellSize))) cell.adjacentCells.Add(fireGrid[pos + new Vector3(0, 0, -cellSize)]);
            if(fireGrid.ContainsKey(pos + new Vector3(0, 0, cellSize))) cell.adjacentCells.Add(fireGrid[pos + new Vector3(0, 0, cellSize)]);
        }

    }

    //Temporary material switch - either make it so that it blends over time or do something fancy with masking shaders
    public void OnFire()
    {
        mat.material.SetFloat("_smoulder", 1);
        mat.material.SetFloat("_burntAmount", 1);
    }


}


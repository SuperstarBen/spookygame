﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{

    public Transform sight;

    private bool canSee;
    public float range = 10f;

    public Transform target;
    private NavMeshAgent agent;
    private LayerMask seeThrough; 

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        
    }

    // Update is called once per frame
    void Update()
    {
        sight.transform.LookAt(target);
        Debug.DrawRay(sight.transform.position, sight.transform.forward *range);

        RaycastHit hit;
        if (Physics.Raycast(sight.transform.position, sight.transform.forward, out hit, range ) && hit.collider.tag == "Player")
        {
            canSee = true;
            agent.SetDestination(target.transform.position);
        }

        else
        {
            canSee = false;
            //agent.ResetPath();
        }

    }
}

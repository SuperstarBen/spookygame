﻿
using UnityEngine;
using InControl;

public class Door : MonoBehaviour
{

    [SerializeField]
    private DoorTrigger trigger;
    [SerializeField]
    private GameObject door;
    [SerializeField]
    private HingeJoint joint;
    [SerializeField]
    private JointLimits limits;
    [SerializeField]
    private float maxOpenAngle;
    [SerializeField]
    private float minOpenAngle;
    [SerializeField]
    private string openAudioEvent;
    [SerializeField]
    private string closeAudioEvent;

    private PlayerCharacterActions characterActions;
    public bool doorOpen;

    public DoorTrigger Trigger { get => trigger; set => trigger = value; }
    public PlayerCharacterActions CharacterActions { get => characterActions; set => characterActions = value; }

    public virtual void DoorToggle()
    {
        if (doorOpen)
        {
            limits.max = 0;
            limits.min = 0;
            joint.limits = limits;
            joint.useSpring = false;
            doorOpen = false;
            AkSoundEngine.PostEvent(closeAudioEvent, this.gameObject);
        }
        else
        {
            limits.max = maxOpenAngle;
            limits.min = minOpenAngle;
            joint.limits = limits;
            joint.useSpring = true;
            doorOpen = true;
            AkSoundEngine.PostEvent(openAudioEvent, this.gameObject );
        }


    }
}


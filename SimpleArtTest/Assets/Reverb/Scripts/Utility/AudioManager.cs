﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Music
{

    public string name;
    public AudioClip clip;

    [Range(0f, 1f)] public float volume = 0.7f;

    [Range(0.5f, 1.5f)] public float pitch = 1;

    [Range(0f, 0.5f)] public float randomVolume = 0.1f;
    [Range(0f, 0.5f)] public float randomPitch = 0.1f;

    public bool loop = false;

    public AudioSource source;

    public void SetSource(AudioSource _source)
    {
        source = _source;
        source.clip = clip;
        source.loop = loop;
    }

    public void Play()
    {
        source.volume = volume * (1 + Random.Range(-randomVolume / 2f, randomVolume / 2f));
        source.pitch = pitch * (1 + Random.Range(-randomPitch / 2f, randomPitch / 2f));
        source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }

}

[System.Serializable]
public class Sound
{

    public string name;
    public AudioClip clip;

    [Range(0f, 1f)] public float volume = 0.7f;

    [Range(0.5f, 1.5f)] public float pitch = 1;

    [Range(0f, 0.5f)] public float randomVolume = 0.1f;
    [Range(0f, 0.5f)] public float randomPitch = 0.1f;

    public bool loop = false;

    public AudioSource source;

    public void SetSource(AudioSource _source)
    {
        source = _source;
        source.clip = clip;
        source.loop = loop;
    }

    public void Play()
    {
        source.volume = volume * (1 + Random.Range(-randomVolume / 2f, randomVolume / 2f));
        source.pitch = pitch * (1 + Random.Range(-randomPitch / 2f, randomPitch / 2f));
        source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }

}

[System.Serializable]
public class SoundGroup
{
    public string name;
    public AudioClip[] clip;

    [Range(0f, 1f)] public float volume = 0.7f;

    [Range(0.5f, 1.5f)] public float pitch = 1;

    [Range(0f, 0.5f)] public float randomVolume = 0.1f;
    [Range(0f, 0.5f)] public float randomPitch = 0.1f;

    public bool loop = false;

    public AudioSource source;

    public void SetSource(AudioSource _source)
    {
        source = _source;
        source.loop = loop;
    }

    public void Play()
    {
        source.volume = volume * (1 + Random.Range(-randomVolume / 2f, randomVolume / 2f));
        source.pitch = pitch * (1 + Random.Range(-randomPitch / 2f, randomPitch / 2f));

        int rnd = Random.Range(0, clip.Length);
        source.clip = clip[rnd];

        source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }

}

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [SerializeField] private Music[] musics;
    [SerializeField] private Sound[] sounds;
    [SerializeField] private SoundGroup[] soundGroups;

    private Dictionary<string, Music> musicDict = new Dictionary<string, Music>();
    private Dictionary<string, Sound> soundDict = new Dictionary<string, Sound>();
    private Dictionary<string, SoundGroup> soundGroupDict = new Dictionary<string, SoundGroup>();

    [HideInInspector] public Music currentMusic = null;
    private bool isMusicPlaying = false;
    public bool IsPlayingMusic { get { return isMusicPlaying; } }

    private Coroutine musicCoroutine;

    void Awake()
    {
        if (instance != null)
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }

        currentMusic = null;
        SetupAudio();
    }

    private void SetupAudio()
    {
        for (int i = 0; i < musics.Length; i++)
        {
            GameObject _go = new GameObject("Music_" + i + "_" + musics[i].name);
            if (musics[i].source == null) musics[i].SetSource(_go.AddComponent<AudioSource>());
            _go.transform.parent = this.transform;

            musicDict.Add(musics[i].name, musics[i]);
        }

        for (int i = 0; i < sounds.Length; i++)
        {
            GameObject _go = new GameObject("Sound_" + i + "_" + sounds[i].name);
            if (sounds[i].source == null) sounds[i].SetSource(_go.AddComponent<AudioSource>());
            _go.transform.parent = this.transform;

            soundDict.Add(sounds[i].name, sounds[i]);
        }

        for (int i = 0; i < soundGroups.Length; i++)
        {
            GameObject _go = new GameObject("SoundGroup_" + i + "_" + soundGroups[i].name);
            if (soundGroups[i].source == null) soundGroups[i].SetSource(_go.AddComponent<AudioSource>());
            _go.transform.parent = this.transform;

            soundGroupDict.Add(soundGroups[i].name, soundGroups[i]);
        }
    }

    public void PlayMusic(string _name)
    {
        Music m = null;

        Debug.Log("Playing " + _name);

        if (currentMusic != null)
        {
            currentMusic.Stop();
            isMusicPlaying = false;
        }

        if (musicDict.TryGetValue(_name, out m))
        {
            currentMusic = m;
            currentMusic.Play();
            isMusicPlaying = true;
            return;
        }

        // no music with _name
        Debug.LogWarning("AudioManager: No music found with name " + _name);
    }

    public void StopMusic()
    {
        if (currentMusic != null)
        {
            currentMusic.Stop();
            isMusicPlaying = false;
            return;
        }

        // no music playing
        Debug.LogWarning("AudioManager: No music currently playing");
    }

    public void PauseMusic()
    {
        if (currentMusic == null)
        {
            Debug.LogWarning("No music found");
            return;
        }

        if (musicCoroutine != null) StopCoroutine(musicCoroutine);
        musicCoroutine = StartCoroutine(PauseMusicCoroutine());
    }

    private IEnumerator PauseMusicCoroutine()
    {
        float counter = 0f;
        float speed = 1f;

        float initialVolume = currentMusic.source.volume;

        while (true)
        {
            counter += Time.deltaTime * speed;
            currentMusic.source.volume = Mathf.Lerp(initialVolume, 0f, counter);

            if (counter >= 1f) break;

            yield return null;
        }
    }

    public void ResumeMusic()
    {
        if (currentMusic == null)
        {
            Debug.LogWarning("No music found");
            return;
        }

        if (musicCoroutine != null) StopCoroutine(musicCoroutine);
        musicCoroutine = StartCoroutine(ResumeMusicCoroutine());
    }

    private IEnumerator ResumeMusicCoroutine()
    {
        float counter = 0f;
        float speed = 1f;

        float initialVolume = currentMusic.source.volume;

        while (true)
        {
            counter += Time.deltaTime * speed;

            currentMusic.source.volume = Mathf.Lerp(initialVolume, 0.2f, counter);

            if (counter >= 1f) break;

            yield return null;
        }
    }

    public void PlaySound(string _name)
    {
        Sound s = null;
        SoundGroup sg = null;

        if (soundDict.TryGetValue(_name, out s))
        {
            s.Play();
            return;
        }

        if (soundGroupDict.TryGetValue(_name, out sg))
        {
            sg.Play();
            return;
        }

        // no sound with _name
        Debug.LogWarning("AudioManager: No sound found with name " + _name);
    }

    public void StopSound(string _name)
    {
        Sound s = null;
        SoundGroup sg = null;
        if (soundDict.TryGetValue(_name, out s))
        {
            s.Stop();
            return;
        }

        if (soundGroupDict.TryGetValue(_name, out sg))
        {
            sg.Stop();
            return;
        }

        // no sound with _name
        Debug.LogWarning("AudioManager: No sound found with name " + _name);
    }

    public void FadeSound(string _name, float fadeTime)
    {
        Sound s = null;
        SoundGroup sg = null;
        if (soundDict.TryGetValue(_name, out s))
        {
            StartCoroutine(FadeOut(s.source, fadeTime));
            return;
        }

        if (soundGroupDict.TryGetValue(_name, out sg))
        {
            StartCoroutine(FadeOut(s.source, fadeTime));
            return;
        }

        // no sound with _name
        Debug.LogWarning("AudioManager: No sound found with name " + _name);
    }

    private IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }
}

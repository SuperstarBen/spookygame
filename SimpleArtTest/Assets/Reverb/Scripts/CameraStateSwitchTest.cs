﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStateSwitchTest : MonoBehaviour
{
    [SerializeField]
    private GameObject camera;

    void Start()
    {
        camera.SetActive(false);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        camera.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        camera.SetActive(false);
    }
}

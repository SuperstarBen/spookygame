﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class InventoryManager : MonoBehaviour
{
    private PlayerCharacterActions pca;
    public InventoryItem focusedItem;
    [SerializeField]

    private void OnEnable()
    {
        pca = PlayerCharacterActions.CreateWithDefaultBindings();
        pca.move.StateThreshold = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        pca.move.Filter(pca.move, Time.deltaTime);

        if(pca.move.Up.WasPressed)
        {
            MoveFocusTo(focusedItem.up);
        }

        if(pca.move.Down.WasPressed)
        {
            MoveFocusTo(focusedItem.down);
        }

        if(pca.move.Left.WasPressed)
        {
            MoveFocusTo(focusedItem.left);
        }

        if(pca.move.Right.WasPressed)
        {
            MoveFocusTo(focusedItem.right);
        }

        if (pca.action.WasPressed)
        {
            Debug.Log(focusedItem);
            FindObjectOfType<PlayerController>().gameObject.SetActive(true);

        }
    }
    
    private void MoveFocusTo(InventoryItem newInventoryItem )
    {
        if (newInventoryItem != null)
        {
            focusedItem = newInventoryItem;
        }
    }
}


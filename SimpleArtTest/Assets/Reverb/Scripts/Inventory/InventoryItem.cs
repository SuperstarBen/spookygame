﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : MonoBehaviour
{
    public InventoryItem up = null;
    public InventoryItem down = null;
    public InventoryItem left = null;
    public InventoryItem right = null;

    private void Update()
    {
        var hasFocus = transform.parent.GetComponent<InventoryManager>().focusedItem == this;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemFocus : MonoBehaviour
{

    void Update()
    {
        var focusedItem = transform.parent.GetComponent<InventoryManager>().focusedItem;

        transform.position = Vector3.MoveTowards(transform.position, focusedItem.transform.position, Time.deltaTime * 10.0f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAudio : MonoBehaviour
{
    private GameObject doorGo;
    private Rigidbody doorRb;
    private HingeJoint doorHj;
    public float minVelocity = 0.01f;
    public float maxVelocity = 1f;
    private float doorVel;
    private bool doorOpen;
    private bool soundReady;


    // Start is called before the first frame update
    void Start()
    {
        doorGo = transform.gameObject;
        doorRb = doorGo.GetComponent<Rigidbody>();
        doorHj = doorGo.GetComponent<HingeJoint>();
       
    }

    // Update is called once per frame
    void Update()
    {
        doorVel = Mathf.Abs(doorHj.velocity);
        
        AkSoundEngine.SetRTPCValue("RelativeVelocityMagnitude", doorVel);
   
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.relativeVelocity.magnitude > minVelocity && collision.relativeVelocity.magnitude < maxVelocity)
        {
            AkSoundEngine.PostEvent("Play_DoorKnock", doorGo);
 
        }
           

    }
}

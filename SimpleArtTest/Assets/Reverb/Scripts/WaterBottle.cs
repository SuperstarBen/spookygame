﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBottle : Item, IUseItem
{

    [SerializeField]
    private float trailInterval = 0.5f;
    [SerializeField]
    private float minDistance = 1f;
    [SerializeField]
    private GameObject trail;
    [SerializeField]
    FireCell fireCell;

    private bool trailing = false;
    private Vector3 currentV3;
    private Vector3 previousV3;
    private int currentIndex;
    private List<Vector3> linePositions = new List<Vector3>();

    private Coroutine lcr;
    private LineRenderer line;

    private void Start()
    {

    }


    IEnumerator LiquidCoroutine()
    {
        while(true)
        {
            yield return Yielders.Get(trailInterval);
            makeTrail();
           
        }
    }

    public void UseItem()
    {
        if (trailing == false)
        {
            
            trailing = true;
            GameObject TrailGo = Instantiate(trail, transform.position, transform.rotation);
            line = TrailGo.GetComponent<LineRenderer>();
            linePositions.Clear();
            if (lcr == null) lcr = StartCoroutine(LiquidCoroutine());
        }
        else
        {
            trailing = false;
            StopCoroutine(lcr);
            lcr = null;
        }
    }

    void makeTrail()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.up * -100);
        Debug.DrawRay(transform.position, transform.up * -100);
        Physics.Raycast(ray, out hit, 100, LayerMask.GetMask("Ground"));
        if(hit.collider !=null) Debug.Log(hit.transform.name);

        currentV3 = hit.point;
        currentV3.y = currentV3.y + .1f;

        float distance = Vector3.Distance(currentV3, previousV3);
        if (distance >= minDistance)
        {
            linePositions.Add(currentV3);
            int count = linePositions.Count;
            Vector3[] linePositionsArray = linePositions.ToArray();
            line.positionCount = count;
            line.SetPositions(linePositionsArray);
            previousV3 = currentV3;
            FireCell cell = (FireCell)Instantiate(fireCell, currentV3, Quaternion.identity);
            cell.transform.SetParent(line.transform);
            cell.Hp = 1;
            cell.Fuel = 30;
            cell.Col.size = new Vector3(1, 1, 1);
            cell.SphereRadius = 0.5f;
        }

    }

}

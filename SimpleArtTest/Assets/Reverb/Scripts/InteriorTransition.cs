﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Wwise;

public class InteriorTransition : MonoBehaviour
{
    public GameObject roof;
    public float speed = 5;
    public Transform focusedTrans;

    private Camera camera;
    private Vector3 startPos;
    private Quaternion startRot;
    private Vector3 targetPos;
    private Quaternion targetRot;


    void Start()
    {
        camera = Camera.main;
        startPos = camera.transform.position;
        startRot = camera.transform.rotation;
        targetPos = startPos;
        targetRot = startRot;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            roof.SetActive(false);
            targetPos = focusedTrans.transform.position;
            targetRot = focusedTrans.transform.rotation;
            AkSoundEngine.SetState("Rain_Ambience", "Inside");


        }

    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player")
        {
            roof.SetActive(true);
            targetPos = startPos;
            targetRot = startRot;
            AkSoundEngine.SetState("Rain_Ambience", "Outside");
        }
         

    }


    private void Update()
    {
       camera.transform.position = Vector3.Lerp(camera.transform.position, targetPos, Time.deltaTime * speed);
       camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, targetRot, Time.deltaTime * speed);

    }

}





// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "FirePM"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_Burnt("Burnt", 2D) = "white" {}
		_BurnAmount("Burn Amount", Range( 0 , 1.3)) = 0
		[HDR]_Hot("Hot", Color) = (0,0,0,0)
		[HDR]_Warm("Warm", Color) = (0,0,0,0)
		_Mask("Mask", 2D) = "white" {}
		_DistortianMap("DistortianMap", 2D) = "bump" {}
		_DistortionAmount("Distortion Amount", Range( 0 , 1)) = 0
		_ScrollSpeed("Scroll Speed", Range( 0 , 1)) = 1
		_HeatWave("Heat Wave", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		AlphaToMask On
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _Burnt;
		uniform float4 _Burnt_ST;
		uniform sampler2D _Mask;
		uniform sampler2D _DistortianMap;
		uniform float _ScrollSpeed;
		uniform float _HeatWave;
		uniform float _BurnAmount;
		uniform float4 _Warm;
		uniform float4 _Hot;
		uniform float4 _DistortianMap_ST;
		uniform float _DistortionAmount;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float2 uv_Burnt = i.uv_texcoord * _Burnt_ST.xy + _Burnt_ST.zw;
			float temp_output_27_0 = ( _Time.y * _ScrollSpeed );
			float2 panner50 = ( temp_output_27_0 * float2( 0,-1 ) + i.uv_texcoord);
			float4 tex2DNode37 = tex2D( _Mask, ( ( (UnpackNormal( tex2D( _DistortianMap, panner50 ) )).xy * _HeatWave ) + i.uv_texcoord ) );
			float temp_output_44_0 = step( tex2DNode37.r , ( _BurnAmount / 1.2 ) );
			float4 lerpResult65 = lerp( tex2D( _Albedo, uv_Albedo ) , tex2D( _Burnt, uv_Burnt ) , temp_output_44_0);
			o.Albedo = lerpResult65.rgb;
			float2 uv_DistortianMap = i.uv_texcoord * _DistortianMap_ST.xy + _DistortianMap_ST.zw;
			float2 panner25 = ( temp_output_27_0 * float2( 0,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord23 = i.uv_texcoord + panner25;
			float4 lerpResult32 = lerp( _Warm , _Hot , tex2D( _Mask, ( ( (UnpackNormal( tex2D( _DistortianMap, uv_DistortianMap ) )).xy * _DistortionAmount ) + uv_TexCoord23 ) ));
			float4 temp_cast_1 = (2.0).xxxx;
			o.Emission = ( ( pow( lerpResult32 , temp_cast_1 ) * 2.0 ) * ( step( tex2DNode37.r , _BurnAmount ) - temp_output_44_0 ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16400
294;-1007;977;967;485.4384;733.9633;2.155557;True;False
Node;AmplifyShaderEditor.SimpleTimeNode;26;-977.4075,772.7027;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-1085.407,856.7027;Float;False;Property;_ScrollSpeed;Scroll Speed;9;0;Create;True;0;0;False;0;1;0.06452925;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;51;-1455.053,1201.082;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-766.4075,805.7027;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;18;-1035.952,370.7008;Float;True;Property;_DistortianMap;DistortianMap;7;0;Create;True;0;0;False;0;2d236a12c4556784fa64eb7a306588c1;2d236a12c4556784fa64eb7a306588c1;True;bump;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.PannerNode;50;-1144.323,1213.154;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;19;-714.0004,379.6739;Float;True;Property;_TextureSample1;Texture Sample 1;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;20;-364.9961,380.6395;Float;False;True;True;False;True;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;25;-560.9406,743.1475;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;49;-783.1674,1275.871;Float;True;Property;_TextureSample4;Texture Sample 4;11;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;22;-354.6553,584.769;Float;False;Property;_DistortionAmount;Distortion Amount;8;0;Create;True;0;0;False;0;0;0.1232518;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;52;-384.7599,1268.706;Float;True;True;True;False;True;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-110.0464,384.0058;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;23;-315.1467,741.4648;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;54;-417.6817,1491.719;Float;False;Property;_HeatWave;Heat Wave;10;0;Create;True;0;0;False;0;0;0.4900962;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;16;-108.3556,150.6033;Float;True;Property;_Mask;Mask;5;0;Create;True;0;0;False;0;d3d06e124864db4498630fb0e21a4e80;d3d06e124864db4498630fb0e21a4e80;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;53;-96.85083,1332.522;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;56;-200.1916,1616.031;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;24;172.8548,483.167;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;57;60.5956,1450.232;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;40;317.8933,1570.275;Float;False;Property;_BurnAmount;Burn Amount;2;0;Create;True;0;0;False;0;0;0.216;0;1.3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;17;229.6792,222.2;Float;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;46;512.7781,1849.573;Float;False;Constant;_EdgeSize;EdgeSize;2;0;Create;True;0;0;False;0;1.2;1.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;31;203.2786,-84.85777;Float;False;Property;_Hot;Hot;3;1;[HDR];Create;True;0;0;False;0;0,0,0,0;2,1.913725,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;70;194.1511,-277.6378;Float;False;Property;_Warm;Warm;4;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0.7169812,0.02867924,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;45;772.7781,1674.573;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;32;520.9528,-127.5658;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;37;261.8351,1321.355;Float;True;Property;_TextureSample2;Texture Sample 2;8;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;36;753.6135,383.0728;Float;False;Constant;_Float0;Float 0;8;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;34;860.8513,22.77283;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;39;696.7303,1297.994;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;60;661.6636,-360.5715;Float;True;Property;_Burnt;Burnt;1;0;Create;True;0;0;False;0;None;bcefb4754815f7243a605dc6b4412c7f;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TexturePropertyNode;43;774.8633,-594.7539;Float;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;False;0;None;662d72b6ec210cf4cbeec2b4d3cb8b2a;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.StepOpNode;44;994.7391,1425.478;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;59;1022.201,-251.1868;Float;True;Property;_TextureSample5;Texture Sample 5;8;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;42;1147.633,-526.8164;Float;True;Property;_TextureSample3;Texture Sample 3;8;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;1003.139,188.1043;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;47;1264.58,1264.818;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;1324.35,416.1349;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;65;1392.471,-110.7222;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1707.245,138.1557;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;FirePM;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;6;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;27;0;26;0
WireConnection;27;1;28;0
WireConnection;50;0;51;0
WireConnection;50;1;27;0
WireConnection;19;0;18;0
WireConnection;20;0;19;0
WireConnection;25;1;27;0
WireConnection;49;0;18;0
WireConnection;49;1;50;0
WireConnection;52;0;49;0
WireConnection;21;0;20;0
WireConnection;21;1;22;0
WireConnection;23;1;25;0
WireConnection;53;0;52;0
WireConnection;53;1;54;0
WireConnection;24;0;21;0
WireConnection;24;1;23;0
WireConnection;57;0;53;0
WireConnection;57;1;56;0
WireConnection;17;0;16;0
WireConnection;17;1;24;0
WireConnection;45;0;40;0
WireConnection;45;1;46;0
WireConnection;32;0;70;0
WireConnection;32;1;31;0
WireConnection;32;2;17;0
WireConnection;37;0;16;0
WireConnection;37;1;57;0
WireConnection;34;0;32;0
WireConnection;34;1;36;0
WireConnection;39;0;37;1
WireConnection;39;1;40;0
WireConnection;44;0;37;1
WireConnection;44;1;45;0
WireConnection;59;0;60;0
WireConnection;42;0;43;0
WireConnection;35;0;34;0
WireConnection;35;1;36;0
WireConnection;47;0;39;0
WireConnection;47;1;44;0
WireConnection;41;0;35;0
WireConnection;41;1;47;0
WireConnection;65;0;42;0
WireConnection;65;1;59;0
WireConnection;65;2;44;0
WireConnection;0;0;65;0
WireConnection;0;2;41;0
ASEEND*/
//CHKSM=13763145F2567A7B46CA7ADB278C5FC924A5FA90
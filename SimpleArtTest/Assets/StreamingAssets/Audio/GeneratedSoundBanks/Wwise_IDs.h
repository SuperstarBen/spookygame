/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_ALLEYRAIN = 1866393387U;
        static const AkUniqueID PLAY_CAR_DOOR_CLOSE = 1738890490U;
        static const AkUniqueID PLAY_CAR_DOOR_OPEN = 454444414U;
        static const AkUniqueID PLAY_CARENGINE = 784600430U;
        static const AkUniqueID PLAY_DOOR_BIG_CLOSE_001 = 1150137872U;
        static const AkUniqueID PLAY_DOORCREAK = 1395412394U;
        static const AkUniqueID PLAY_DOORKNOCK = 543223178U;
        static const AkUniqueID PLAY_FREEZER_HUMMING_SOUND_EFFECT = 3815870605U;
        static const AkUniqueID PLAY_INSIDERAIN = 1364240704U;
        static const AkUniqueID PLAY_OUTSIDERAIN = 940963925U;
        static const AkUniqueID PLAY_ROOM_TONE_SMALL_SIZED_ROOM_2_5_1_LCRLSRSLFE = 2142038818U;
        static const AkUniqueID SETSTATE = 293623666U;
        static const AkUniqueID STOP_INSIDERAIN = 3141500806U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace RAIN_AMBIENCE
        {
            static const AkUniqueID GROUP = 271688190U;

            namespace STATE
            {
                static const AkUniqueID INSIDE = 3553349781U;
                static const AkUniqueID OUTSIDE = 438105790U;
            } // namespace STATE
        } // namespace RAIN_AMBIENCE

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID CARRPM = 2563310688U;
        static const AkUniqueID RELATIVEVELOCITYMAGNITUDE = 319209282U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID LARGEROOM = 187046019U;
        static const AkUniqueID SMALLROOM = 2933838247U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
